import java.util.*;

class KeyWord {
  
  Set<String> words;
  int mentions;
  int people;
  int rate;
  int index;
  
  KeyWord(String inWord, int inIndex, int inMentions, int inPeople){
    this.index    = inIndex;
    this.mentions = inMentions;
    this.people   = inPeople;
    this.rate     = (int) floor(this.mentions / this.people);
    this.words    = new HashSet();
    this.add(inWord);
  }
  
  void add(String inWord){
    this.words.add(inWord);
  }
  
  void setIndex(int inIndex){
    this.index = inIndex;
  }
  
  Set<String> getWords(){
    return this.words;
  }
  
  int getMentions(){
    return mentions;
  }

  int getPeople(){
    return people;
  }
  
  int getRate(){
    return rate;
  }
  
  int getSize(){
    return this.words.size();
  }
  
  int getIndex(){
    return this.index;
  }  
  
}

class MentionsComparator implements Comparator<KeyWord> {
  public int compare(KeyWord k1, KeyWord k2) {
      return k1.getMentions() - k2.getMentions();
  }
}

class PeopleComparator implements Comparator<KeyWord> {
  public int compare(KeyWord k1, KeyWord k2) {
      return k1.getPeople() - k2.getPeople();
  }
}

class RateComparator implements Comparator<KeyWord> {
  public int compare(KeyWord k1, KeyWord k2) {
      return k1.getRate() - k2.getRate();
  }
}

class SizeComparator implements Comparator<KeyWord> {
  public int compare(KeyWord k1, KeyWord k2) {
      return k1.getSize() - k2.getSize();
  }
}
