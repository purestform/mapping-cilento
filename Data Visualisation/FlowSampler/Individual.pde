import java.util.*;


class Individual {

  List<Entry> entries;
  int col = -1;
  
  Individual(){
    entries = new ArrayList<Entry>();
  }
  
  Individual(int inputColor){
    entries = new ArrayList<Entry>();
    col = inputColor;
  }
  
  void add(Entry inputEntry){
    entries.add(inputEntry);
  }
  
  int getColor(){
    return col;
  }
  
  int size(){
    return entries.size();
  }
  
  List<Entry> getEntries(){
    return entries;
  }
  
  Entry get(int inputIndex){
    Entry entry = (Entry) entries.get(inputIndex);
    return entry;
  }
  
  int getNumEntriesByDateTime(DateRange inputRange){
    int count = 0;
    for(Entry e : entries){
      if(e.date.after(inputRange.getFrom()) && 
         e.date.before(inputRange.getTill())){
        count++;
      }
    }
    return count;
  }
  
  List<Entry> getEntriesByDateTime(DateRange inputRange){
    List<Entry> output = new ArrayList<Entry>();
    for(Entry e : entries){
      if(e.date.after(inputRange.getFrom()) && 
         e.date.before(inputRange.getTill())){
        output.add(e);
      }
    }
    return output;
  }
  
  List<Entry> getEntriesByDate(DateRange inputRange){
    List<Entry> output = new ArrayList<Entry>();
    Calendar c = Calendar.getInstance();
    c.setTime(inputRange.getFrom());
    int dayFrom = c.get(Calendar.DAY_OF_YEAR);
    c.setTime(inputRange.getTill());
    int dayTill = c.get(Calendar.DAY_OF_YEAR);
    for(Entry e : entries){
      c.setTime(e.date);
      int day = c.get(Calendar.DAY_OF_YEAR);
      if(day <= dayTill && day >= dayFrom){
        output.add(e);
      }
    }
    return output;
  }
  
  List<Entry> getEntriesByTime(DateRange inputRange){
    List<Entry> output = new ArrayList<Entry>();
    Calendar c = Calendar.getInstance();
    c.setTime(inputRange.getFrom());
    int day = c.get(Calendar.DAY_OF_YEAR); 
    for(Entry e : entries){
      c.setTime(e.date);
      c.set(Calendar.DAY_OF_YEAR, day);
      Date date = c.getTime();
      if(date.after(inputRange.getFrom()) && 
         date.before(inputRange.getTill())){
        output.add(e);
      }
    }
    return output;
  }
  
}

