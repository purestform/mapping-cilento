import geotools.geo.*;
import geotools.maps.*;
import geotools.providers.*;
import geotools.projections.*;


class DraggableMap {

  InteractiveMap map;
  
  boolean isDrag  = false;
  boolean isSleep = false;
  
  double tx;
  double ty;
  double sc;
  
  int wheelTracker = 0;
  int frameTracker = 0;
  int sleepTracker = 0;
  int sleepThreshold = 120;
  
  DraggableMap(PApplet inApplet, Location inCentre, int inZoom){
    
//    // Online Satellite Photo
//    this.map = new InteractiveMap(inApplet, new Microsoft.AerialProvider());
//    this.map.setCenterZoom(inCentre, inZoom);

  String[] subdomains = new String[] { "a." };
  String img = "{Z}/{X}/{Y}@2x.png";
  String template = "http://stamen-tiles." + subdomains[0] + "ssl.fastly.net/toner-lite/" + img;
  this.map = new InteractiveMap(inApplet, new TemplatedMapProvider(template, subdomains));
  this.map.setCenterZoom(inCentre, inZoom);
    
  }
  
  void draw(){
    this.sleepStatus();
    this.wheelStatus();
    tint(255,120);
    this.map.draw(0,0);
    noTint();
    if(this.map.queue.size() == 0){
      this.sleepTracker++;
    }
  }
  
  void sleepStatus(){
    if(this.sleepTracker >= this.sleepThreshold){
      this.isSleep = true;
    }
  }
  
  void wheelStatus(){
    if(this.wheelTracker == 2){
      this.wheelTracker = 0;
    } else if(this.wheelTracker == 1){
      if((frameTracker + (frameRate/2)) < frameCount){
        this.wheelTracker = 2;
      }
    }
  }
  
  void mousePressed(){
    this.isSleep = false;
    this.sleepTracker = 0;
  }

  void mouseDragged(){
    this.isDrag = true;
    this.isSleep = false;
    this.sleepTracker = 0;
    this.map.mouseDragged();
  }
  
  void mouseReleased(){
    if(this.isDrag){
      this.isDrag = false;
    }
  }
  
  void mouseWheel(float inDelta){
    this.wheelTracker = 1;
    this.frameTracker = frameCount;
    this.sleepTracker = 0;
    this.isSleep = false;
    
    float threshold = 1.0;
    if (inDelta < 0) {
      threshold = 1.05;
    } else if (inDelta > 0) {
      threshold = 1.0/1.05; 
    }
    
    float mx = mouseX - width/2;
    float my = mouseY - height/2;
    this.map.tx -= mx/this.map.sc;
    this.map.ty -= my/this.map.sc;
    this.map.sc *= threshold;
    this.map.tx += mx/this.map.sc;
    this.map.ty += my/this.map.sc;
  }

  void setZoomSC(float inZoomSC){
    map.setZoomSC(inZoomSC);
  }
  
  void setCentre(Location inLoc, int inZoom){
    map.setCenterZoom(inLoc, inZoom);
  }
  
  boolean isDrag(){
    return this.isDrag;
  }
  
  boolean isSleep(){
    return this.isSleep;
  }  
  
  int getWheelStatus(){
    return this.wheelTracker;
  }
  
  PVector locationPoint(Location inLocation){
    return this.map.locationPoint(inLocation);
  }
  
  Location pointLocation(PVector inPVector){
    return this.map.pointLocation(inPVector);
  }  
  
  Location getCenter(){
    return this.map.getCenter();
  }

  int getZoom(){
    return this.map.getZoom();
  }

  float getZoomSC(){
    return this.map.getZoomSC();
  }
  
}
