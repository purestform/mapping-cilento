import java.util.*;
import java.text.*;

class TimeLine extends Histogram {
  
  List<Date> dayofyears;
  List<Integer> selected;
  List<Integer> incoming;
  List<Integer> outgoing;
  float halfHeight;
  float[] sel_bar_height = null;  
  int sel_max_value = 0;
  int sel_min_value = 0;
  
  TimeLine(List<String> keys, List<Date> indayofyears, List<Integer> values, List<Integer> inselected, boolean isIntKey){
    super(keys, values, isIntKey);
    this.dayofyears = indayofyears;
    this.selected = inselected;
    this.incoming = new ArrayList();
    this.outgoing = new ArrayList();
  }
  
  Histogram setup(int x, int y, int w, int h, boolean isInit){
    
    rect = new Rectangle(x, y, w, h);
    hover_area = new Rectangle(x, y - 10, w, 10);
    //bar width
    bar_width = (float) w / (float)keys.size();
    //bar height
    bar_height = new float[keys.size()];
    sel_bar_height = new float[keys.size()];

//    // **** Non Polar Histogram
//    this.max_value = Collections.max(values);
//    this.min_value = Collections.min(values); 
//    for(int i = 0; i < values.size(); i++){
//      int value = values.get(i).intValue();
//      if(value > 0) bar_height[i] = map(value, min_value, max_value, 0, rect.height);
//    }
//    // **** Non Polar Histogram

    // **** Polar Histogram
    this.max_value = Collections.max(values);
    this.min_value = Collections.min(values); 
    this.sel_max_value = Collections.max(selected);
    this.sel_min_value = Collections.min(selected);
    if(!isInit){
      float selValHtPercentage = map(sel_max_value, min_value, max_value, 0, 1);
      float maxSelValHt = constrain(map(selValHtPercentage, 0, 1, rect.height*.1, rect.height*.5), rect.height*.1, rect.height*.5);
      this.halfHeight = rect.height - maxSelValHt;
      println(maxSelValHt + "/" + halfHeight);
      for(int i = 0; i < values.size(); i++){
        int value = values.get(i).intValue();
        if(value > 0) bar_height[i] = map(value, min_value, max_value, 0, halfHeight);
        int sel_value = selected.get(i).intValue();
        if(sel_value > 0){
          //sel_bar_height[i] = constrain((map(sel_value, min_value, max_value, 0, rect.height)), 0, maxSelValHt);
          sel_bar_height[i] = map(sel_value, sel_min_value, sel_max_value, 1, maxSelValHt);
          //println(sel_bar_height[i]);
        }
      }
    } else {
      float selValHtPercentage = 0;
      float maxSelValHt = 0;
      this.halfHeight = rect.height - maxSelValHt;
      println(maxSelValHt + "/" + halfHeight);
      for(int i = 0; i < values.size(); i++){
        int value = values.get(i).intValue();
        if(value > 0) bar_height[i] = map(value, min_value, max_value, 0, halfHeight);
        int sel_value = selected.get(i).intValue();
        if(sel_value > 0){
          //sel_bar_height[i] = constrain((map(sel_value, min_value, max_value, 0, rect.height)), 0, maxSelValHt);
          sel_bar_height[i] = map(sel_value, sel_min_value, sel_max_value, 1, maxSelValHt);
          //println(sel_bar_height[i]);
        }
      }
    }
    // **** Polar Histogram

    //font
    font = createFont("supernatural10.ttf",10);
    textFont(font);
    //handle position
    handle_x_left = rect.x + (bar_width*index_start);
    handle_x_right = rect.x + (bar_width*index_end );
    return this;
    
  }  


  //draw function
  void display(){
    
    //background
    fill(color_background);
    noStroke();
    rect(rect.x, rect.y, rect.width, rect.height);

    //bars
    noStroke();
    float bar_x = rect.x;
    for(int i = 0; i < values.size(); i++){
      if(i >= index_start && i < index_end){
        fill(color_bar_selected);
      } else {
        fill(color_bar_unselected);
      }
      
      // polar histogram
      rect(bar_x, rect.y + this.halfHeight - bar_height[i], bar_width, bar_height[i]);
      
      // non polar histogram
      //rect(bar_x, rect.y + rect.height - bar_height[i], bar_width, bar_height[i]);
      
      if(i >= index_start && i < index_end){
        fill(150);
      } else {
        fill(color_bar_unselected);
      }
      rect(bar_x, rect.y + this.halfHeight, bar_width, sel_bar_height[i]);
      bar_x += bar_width;
    }

    //x axis
    fill(color_text);
    noStroke();
    float runningX = rect.x + bar_width/2;
    float runningY = rect.y + rect.height;
    
    List<String> renderLabels = keys;
    if(labels != null) renderLabels = labels;
    
    for(int i = 0; i < renderLabels.size(); i++){
      if(i%3 == 0){
        String label = renderLabels.get(i);
        //rotating label if it is too long
        if(textWidth(label)+10 > bar_width){
          if(label.length()>10){
            label = label.substring(0,9)+"...";
          }
          pushMatrix();
          translate(runningX, runningY);
          rotate(- HALF_PI/2);
          textAlign(RIGHT, CENTER);
          text(label, 0, 0);
          popMatrix();
        }else{
          textAlign(CENTER, TOP);
          text(label, runningX, runningY);
        }
      }
      runningX += bar_width;
    }

    //y axis
    fill(color_text);
    noStroke();
    runningX = rect.x;
    runningY = rect.y + this.halfHeight;
    //min
    textAlign(RIGHT, CENTER);
    text(min_label, runningX, runningY);
    //max
    textAlign(RIGHT, TOP);
    runningY -= this.halfHeight;
    text(max_value, runningX, runningY);
    //field name
    textAlign(RIGHT, BOTTOM);
    text(sel_max_value, runningX, rect.y + rect.height);

    //draw selected range
    stroke(color_handle);
    line(handle_x_left, rect.y - handle_height, handle_x_left, rect.y + rect.height);
    line(handle_x_right, rect.y - handle_height, handle_x_right, rect.y + rect.height);

    //mouse hovering
    if(hover_area.contains(mouseX, mouseY)){
      if(isMiddleSelected){
        //range is selected
        noStroke();
        fill(color_blue);
        rect(handle_x_left, hover_area.y, (index_end-index_start)*bar_width, hover_area.height);
      }else{
        //draw triangle
        //left
        noStroke();
        fill(abs(mouseX - handle_x_left)< handle_height || isLeftSelected ? color_blue: color_handle);
        triangle(handle_x_left, hover_area.y, handle_x_left, hover_area.y+hover_area.height, handle_x_left+ handle_height, (float)hover_area.getCenterY());
        //right
        fill(abs(mouseX - handle_x_right)< handle_height || isRightSelected ? color_blue: color_handle);
        triangle(handle_x_right, hover_area.y, handle_x_right, hover_area.y+hover_area.height, handle_x_right - handle_height, (float)hover_area.getCenterY());        
      }
    }
    
  }
  
  //get active keys
  Date[] getActiveRange(){
    List<Date> result = new ArrayList<Date>();
    for(int i = index_start; i < index_end; i++){
      result.add(this.dayofyears.get(i));
    }
    Collections.sort(result);
    Date fr = (Date) result.get(0);
    Date to = (Date) result.get(result.size()-1);
    Date[] output = {fr, to};
    return output;
  }

}
