import java.util.*;
import geotools.geo.*;
import org.apache.commons.lang3.builder.EqualsBuilder;


class Entry {

  Date date;
  Object individualId;
  Location location;
  PVector position;
  boolean isFlag;
  String[] keywords;
  
  Entry(Object inputIndividual, Date inputDate, Location inputLocation, String[] inKeywords, boolean inFlag){
    individualId = inputIndividual;
    date         = inputDate;
    location     = inputLocation;
    keywords     = inKeywords;
    isFlag       = inFlag;
  }  
  
  Entry(Object inputIndividual, Date inputDate, Location inputLocation, boolean inFlag){
    individualId = inputIndividual;
    date         = inputDate;
    location     = inputLocation;
    isFlag       = inFlag;
  }
  
  Entry(Object inputIndividual, Date inputDate, Location inputLocation){
    individualId = inputIndividual;
    date         = inputDate;
    location     = inputLocation;
  }
  
  Object getIndividualId(){
    return this.individualId;
  }
  
  Location getLocation(){
    return this.location;
  }
  
  Date getDate(){
    return date;
  }
  
  void setPosition(PVector inputPosition){
    position = inputPosition.get();
  }
  
  PVector getPosition(){
    return position;
  }
  
  @Override
  boolean equals(Object obj) {
      if (obj instanceof Entry) {
        Entry other = (Entry) obj;
          EqualsBuilder builder = new EqualsBuilder();
          builder.append(date, other.date);
          return builder.isEquals();
      }
      return false;
  }
  
  int[] getHourMin(){
    Calendar loCal = Calendar.getInstance();
    loCal.setTime(date);
    int hour = loCal.get(Calendar.HOUR_OF_DAY);
    int minutes = loCal.get(Calendar.MINUTE);
    int[] output = {hour,minutes};
    return output;
  }
  
}


public class EntrySorter implements Comparator<Object> {

  @Override
  public int compare(Object o1, Object o2) {
    if(o1 != null && o2 != null){
      Entry e1 = (Entry) o1;
      Entry e2 = (Entry) o2;
      return e1.getDate().compareTo(e2.getDate());
    } else {
      return 1;
    }
  }

}

