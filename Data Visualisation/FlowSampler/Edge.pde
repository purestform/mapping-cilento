class Edge {

  Location from;
  Location to;
  int distance;
  
  List<Entry[]> entries;
  Set<Object> individuals;
  
  Edge(Location inputFrom, Location inputTo){
    this.from = inputFrom;
    this.to = inputTo;
    this.entries = new ArrayList();
    this.individuals = new HashSet();
    this.distance = (int) ceil(this.from.distanceTo(this.to));
    if(this.distance < 1) this.distance = 1;
  }
  
  void setEntries(List<Entry[]> inEntries){
    this.entries.addAll(inEntries);
  }
  
  void setIndividuals(Set<Object> inIndividuals){
    this.individuals.addAll(inIndividuals);
  }   
  
  void addEntries(Entry[] inEntries){
    this.entries.add(inEntries);
  }
  
  int getNumEntries(){
    return this.entries.size(); 
  }
  
  int getNumIndividuals(){
    return this.individuals.size(); 
  }
  
  int getDistance(){
    return this.distance; 
  }  
  
  List<Entry[]> getEntries(){
    return this.entries; 
  }
  
  Set<Object> getIndividuals(){
    return this.individuals; 
  }  

  @Override
   public boolean equals(Object inputObj) {
     if (!(inputObj instanceof Edge))
       return false;
     final Edge e = (Edge) inputObj;
     return this.from.lat == e.from.lat && 
            this.from.lon == e.from.lon && 
            this.to.lat == e.to.lat && 
            this.to.lon == e.to.lon;
  }
  
  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + Float.floatToIntBits(this.from.lat);
    result = 31 * result + Float.floatToIntBits(this.from.lon);
    result = 31 * result + Float.floatToIntBits(this.to.lat);
    result = 31 * result + Float.floatToIntBits(this.to.lon);    
    return result;
  }  
  
}







Set<Edge> getEdgesByDay(Map<Location, Set<Entry>> inputClusters){
  
  List<Entry> entries = new ArrayList();
  for(Location loc : inputClusters.keySet()){
    Set<Entry> clusterEntries = (HashSet) inputClusters.get(loc);
    Iterator it = clusterEntries.iterator();
    while(it.hasNext()){
      Entry e = (Entry) it.next();
      entries.add(e);
    }
  }
  
  Map<Entry,Location> clusterReference = new HashMap();
  
  List<Location> clusterKeys = new ArrayList(inputClusters.keySet());
  for(int i=0; i<clusterKeys.size(); i++){
    Location loc = (Location) clusterKeys.get(i);
    Set<Entry> cEntries = (HashSet) inputClusters.get(loc);
    Iterator it = cEntries.iterator();
    while(it.hasNext()){
      Entry e = (Entry) it.next();
      clusterReference.put(e, loc);
    }
  }
  
  Map<Edge,List<Entry[]>> edges = new HashMap();
  
  // Sort & weigh edges by day 
  // Edges are constructed out of
  // a pair of tweets made 
  // between 00:00 -> 23:59
  Calendar cal = Calendar.getInstance();
  Collections.sort(entries, new EntrySorter());
  Map<Integer,List<Entry>> ordered = new HashMap();
  for(Entry e : entries){
    Date d = e.getDate();
    cal.setTime(d);
    int day = cal.get(Calendar.DAY_OF_YEAR);
    if(ordered.containsKey(day)){
      List<Entry> dayEntries = (ArrayList) ordered.get(day);
      dayEntries.add(e);
    } else {
      List<Entry> dayEntries = new ArrayList();
      dayEntries.add(e);
      ordered.put(day, dayEntries);
    }
  }
  
  
  Collections.sort(entries, new EntrySorter());
  
  List<Integer> byDay = new ArrayList(ordered.keySet());
  Collections.sort(byDay);
  for(Integer day : byDay){
    List<Entry> dayEntries = (ArrayList) ordered.get(day);
    for(int i=0; i<dayEntries.size()-1; i++){
      Entry e1 = (Entry) entries.get(i);
      Entry e2 = (Entry) entries.get(i + 1);
      Location l1 = (Location) clusterReference.get(e1);
      Location l2 = (Location) clusterReference.get(e2);
      Edge edge = new Edge(l1, l2);
      if(edges.containsKey(edge)){
        List<Entry[]> weight = (ArrayList) edges.get(edge);
        Entry[] frmTo = new Entry[2];
        frmTo[0] = e1;
        frmTo[1] = e2;
        weight.add(frmTo);
      } else {
        List<Entry[]> weight = new ArrayList();
        Entry[] frmTo = new Entry[2];
        frmTo[0] = e1;
        frmTo[1] = e2;
        weight.add(frmTo);
        edges.put(edge,weight);
      }
    }
  }


//  // Sort & weigh edges by day 
//  // Edges are constructed out of
//  // a pair of tweets made 
//  // between 06:00 -> 05:59 (Next day)
//  Calendar cal = Calendar.getInstance();
//  Collections.sort(entries, new EntrySorter());
//  Map<Integer,List<Entry>> ordered = new HashMap();
//  for(Entry e : entries){
//    Date d = e.getDate();
//    cal.setTime(d);
//    int day  = cal.get(Calendar.DAY_OF_YEAR);
//    if(ordered.containsKey(day)){
//      List<Entry> dayEntries = (ArrayList) ordered.get(day);
//      dayEntries.add(e);
//    } else {
//      List<Entry> dayEntries = new ArrayList();
//      dayEntries.add(e);
//      ordered.put(day, dayEntries);
//    }
//  }
//  
//  Collections.sort(entries, new EntrySorter());
//  
//  List<Integer> byDay = new ArrayList(ordered.keySet());
//  Collections.sort(byDay);
//  for(int n=1; n<byDay.size(); n++){
//    int day1 = (Integer) byDay.get(n-1);
//    int day2 = (Integer) byDay.get(n);
//    List<Entry> dayEntries1 = (ArrayList) ordered.get(day1);
//    List<Entry> dayEntries2 = (ArrayList) ordered.get(day2);
//    for(int i=0; i<dayEntries1.size()-1; i++){
//      Entry e1 = (Entry) entries.get(i);
//      Entry e2 = (Entry) entries.get(i + 1);
//      Date d1 = e1.getDate();
//      cal.setTime(d1);
//      int hour1 = cal.get(Calendar.HOUR_OF_DAY);
//      Date d2 = e2.getDate();
//      cal.setTime(d2);
//      int hour2 = cal.get(Calendar.HOUR_OF_DAY);
//      if(hour1 >= 6 && hour2 >= 6){
//        Location l1 = (Location) clusterReference.get(e1);
//        Location l2 = (Location) clusterReference.get(e2);
//        Edge edge = new Edge(l1, l2);
//        if(edges.containsKey(edge)){
//          List<Entry[]> weight = (ArrayList) edges.get(edge);
//          Entry[] frmTo = new Entry[2];
//          frmTo[0] = e1;
//          frmTo[1] = e2;
//          weight.add(frmTo);
//        } else {
//          List<Entry[]> weight = new ArrayList();
//          Entry[] frmTo = new Entry[2];
//          frmTo[0] = e1;
//          frmTo[1] = e2;
//          weight.add(frmTo);
//          edges.put(edge,weight);
//        }
//      }
//    }
//    for(int i=0; i<dayEntries2.size()-1; i++){
//      Entry e1 = (Entry) entries.get(i);
//      Entry e2 = (Entry) entries.get(i + 1);
//      Date d1 = e1.getDate();
//      cal.setTime(d1);
//      int hour1 = cal.get(Calendar.HOUR_OF_DAY);
//      Date d2 = e2.getDate();
//      cal.setTime(d2);
//      int hour2 = cal.get(Calendar.HOUR_OF_DAY);
//      if(hour1 < 6 && hour2 < 6){
//        Location l1 = (Location) clusterReference.get(e1);
//        Location l2 = (Location) clusterReference.get(e2);
//        Edge edge = new Edge(l1, l2);
//        if(edges.containsKey(edge)){
//          List<Entry[]> weight = (ArrayList) edges.get(edge);
//          Entry[] frmTo = new Entry[2];
//          frmTo[0] = e1;
//          frmTo[1] = e2;
//          weight.add(frmTo);
//        } else {
//          List<Entry[]> weight = new ArrayList();
//          Entry[] frmTo = new Entry[2];
//          frmTo[0] = e1;
//          frmTo[1] = e2;
//          weight.add(frmTo);
//          edges.put(edge,weight);
//        }
//      }
//    }   
//  }
  
  for(Edge e : edges.keySet()){
    List<Entry[]> indEntries = (ArrayList) edges.get(e);
    e.setEntries(indEntries);
  }
  
  return new HashSet(edges.keySet());
  
}


Map<Object,Set<Edge>> getEdgesByDay(Map<Location,Map<Object,Set<Entry>>> inputClusters){

  Map<Object,Map<Location,Set<Entry>>> indClusterMap = new HashMap(); 
  
  for(Location loc : inputClusters.keySet()){
    Map<Object,Set<Entry>> indEntryMap = (HashMap) inputClusters.get(loc);
    for(Object id : indEntryMap.keySet()){
      Set<Entry> clusterEntries = (HashSet) indEntryMap.get(id);
      if(indClusterMap.containsKey(id)){
        Map<Location,Set<Entry>> clusters = (HashMap) indClusterMap.get(id);
        clusters.put(loc,clusterEntries);
      } else {
        Map<Location,Set<Entry>> clusters = new HashMap();
        clusters.put(loc,clusterEntries);
        indClusterMap.put(id,clusters);
      }
    }
  }
  
  Map<Object, Set<Edge>> indEdgeMap = new HashMap(); // all trips per person
  Map<Edge, List<Entry[]>> aggEdges = new HashMap(); // all the tweets between edges
  
  for(Object id : indClusterMap.keySet()){
    Map<Location,Set<Entry>> clusters = (HashMap) indClusterMap.get(id);
    Set<Edge> edges = getEdgesByDay(clusters);
    indEdgeMap.put(id, edges);
    Iterator it = edges.iterator();
    while(it.hasNext()){
      Edge e = (Edge) it.next();
      if(aggEdges.containsKey(e)){
        List<Entry[]> weight = (ArrayList) aggEdges.get(e);
        weight.addAll(e.getEntries());
      } else {
        aggEdges.put(e, e.getEntries());
      }
    }
  }
  
//  for(Object id : indEdgeMap.keySet()){
//    Set<Edge> edges = (HashSet) indEdgeMap.get(id);
//    Iterator it = edges.iterator();
//    while(it.hasNext()){
//      Edge e = (Edge) it.next();
//      List<Entry[]> entries = (ArrayList) aggEdges.get(e);
//      e.setEntries(entries);
//    }
//  }
  
  for(Object id : indEdgeMap.keySet()){
    Set<Edge> edges = (HashSet) indEdgeMap.get(id);
    Iterator it = edges.iterator();
    while(it.hasNext()){
      Edge e = (Edge) it.next();
      if(e.getNumIndividuals() == 0){
        List<Entry[]> weight = (ArrayList) aggEdges.get(e);
        Set<Object> ids = new HashSet();
        for(Entry[] entryPair : weight){
          ids.add(entryPair[0].getIndividualId());
        }
        e.setIndividuals(ids);
      }
    }
  }
  
  return indEdgeMap;
  
}








Set<Edge> getEdgesByInterval(Map<Location, Set<Entry>> inputClusters, int inputDuration){
  
  List<Entry> entries = new ArrayList();
  for(Location loc : inputClusters.keySet()){
    Set<Entry> clusterEntries = (HashSet) inputClusters.get(loc);
    Iterator it = clusterEntries.iterator();
    while(it.hasNext()){
      Entry e = (Entry) it.next();
      entries.add(e);
    }
  }
  
  Map<Entry,Location> clusterReference = new HashMap();
  
  List<Location> clusterKeys = new ArrayList(inputClusters.keySet());
  for(int i=0; i<clusterKeys.size(); i++){
    Location loc = (Location) clusterKeys.get(i);
    Set<Entry> cEntries = (HashSet) inputClusters.get(loc);
    Iterator it = cEntries.iterator();
    while(it.hasNext()){
      Entry e = (Entry) it.next();
      clusterReference.put(e, loc);
    }
  }
  
  Map<Edge,List<Entry[]>> edges = new HashMap();
  // Sort & weigh edges by time difference
  // Edges are constructed out of a pair of
  // tweets that meet a minimum 
  // time difference between them
  Calendar cal = Calendar.getInstance();  
  Collections.sort(entries, new EntrySorter());
  
  for(int i=0; i<entries.size()-1; i++){
    Entry e1 = (Entry) entries.get(i);
    Entry e2 = (Entry) entries.get(i + 1);
    Date d1 = e1.getDate();
    Date d2 = e2.getDate();
    cal.setTime(d1);
    int h1 = cal.get(Calendar.HOUR);
    int m1 = cal.get(Calendar.MINUTE);
    int v1 = (h1 * 60) + m1;
    cal.setTime(d2);
    int h2 = cal.get(Calendar.HOUR);
    int m2 = cal.get(Calendar.MINUTE);
    int v2 = (h2 * 60) + m2;
    int vd = abs(v2 - v1);
    if(vd <= inputDuration){
      Location l1 = (Location) clusterReference.get(e1);
      Location l2 = (Location) clusterReference.get(e2);
      Edge edge = new Edge(l1, l2);
      if(edges.containsKey(edge)){
        List<Entry[]> weight = (ArrayList) edges.get(edge);
        Entry[] frmTo = new Entry[2];
        frmTo[0] = e1;
        frmTo[1] = e2;
        weight.add(frmTo);
      } else {
        List<Entry[]> weight = new ArrayList();
        Entry[] frmTo = new Entry[2];
        frmTo[0] = e1;
        frmTo[1] = e2;
        weight.add(frmTo);
        edges.put(edge,weight);
      }
    }
  }
  
  for(Edge e : edges.keySet()){
    List<Entry[]> indEntries = (ArrayList) edges.get(e);
    e.setEntries(indEntries);
  }  
  
  return new HashSet(edges.keySet());
  
}








Map<Object,Set<Edge>> getEdgesByInterval(Map<Location,Map<Object,Set<Entry>>> inputClusters, int inputDuration){

  Map<Object,Map<Location,Set<Entry>>> indClusterMap = new HashMap(); 
  
  for(Location loc : inputClusters.keySet()){
    Map<Object,Set<Entry>> indEntryMap = (HashMap) inputClusters.get(loc);
    for(Object id : indEntryMap.keySet()){
      Set<Entry> clusterEntries = (HashSet) indEntryMap.get(id);
      if(indClusterMap.containsKey(id)){
        Map<Location,Set<Entry>> clusters = (HashMap) indClusterMap.get(id);
        clusters.put(loc,clusterEntries);
      } else {
        Map<Location,Set<Entry>> clusters = new HashMap();
        clusters.put(loc,clusterEntries);
        indClusterMap.put(id,clusters);
      }
    }
  }
  
  Map<Object, Set<Edge>> indEdgeMap = new HashMap(); // all trips per person
  Map<Edge, List<Entry[]>> aggEdges = new HashMap(); // all the tweets between edges
  
  for(Object id : indClusterMap.keySet()){
    Map<Location,Set<Entry>> clusters = (HashMap) indClusterMap.get(id);
    Set<Edge> edges = getEdgesByInterval(clusters, inputDuration);
    indEdgeMap.put(id, edges);
    Iterator it = edges.iterator();
    while(it.hasNext()){
      Edge e = (Edge) it.next();
      if(aggEdges.containsKey(e)){
        List<Entry[]> weight = (ArrayList) aggEdges.get(e);
        weight.addAll(e.getEntries());
      } else {
        aggEdges.put(e, e.getEntries());
      }
    }
  }
  
  for(Object id : indEdgeMap.keySet()){
    Set<Edge> edges = (HashSet) indEdgeMap.get(id);
    Iterator it = edges.iterator();
    while(it.hasNext()){
      Edge e = (Edge) it.next();
      List<Entry[]> entries = (ArrayList) aggEdges.get(e);
      e.setEntries(entries);
    }
  }
  
  for(Object id : indEdgeMap.keySet()){
    Set<Edge> edges = (HashSet) indEdgeMap.get(id);
    Iterator it = edges.iterator();
    while(it.hasNext()){
      Edge e = (Edge) it.next();
      if(e.getNumIndividuals() == 0){
        List<Entry[]> weight = (ArrayList) aggEdges.get(e);
        Set<Object> ids = new HashSet();
        for(Entry[] entryPair : weight){
          ids.add(entryPair[0].getIndividualId());
        }
        e.setIndividuals(ids);
      }
    }
  }
  
  return indEdgeMap;
  
}






boolean isOnLine(PVector v0, PVector v1, PVector p, PVector vp, int closeness) {
  PVector line = PVector.sub(v1, v0);
  float l2 = line.magSq();  // i.e. |w-v|^2 -  avoid a sqrt
  if (l2 == 0.0) {
    vp.set(v0);
    return false;
  }
  PVector pv0_line = PVector.sub(p, v0);
  float t = pv0_line.dot(line)/l2;
  pv0_line.normalize();
  vp.set(line);
  vp.mult(t);
  vp.add(v0);
  float d = PVector.dist(p, vp);
  if (t >= 0 && t <= 1 && d <= closeness)
    return true;
  else
    return false;
}
