import java.text.*;
import java.util.*;


class DateRange {

  Date from;
  Date till;

  DateRange(Date inputFrom, Date inputTill){
    from = inputFrom;
    till = inputTill;
  }

  DateRange(String inputFrom, String inputTill){
    try {
      from = stringWildCard(inputFrom);
      till = stringWildCard(inputTill);
    } catch (ParseException e) {
      println(e);
    }
  }
  
  void setFrom(Date inputDate){
    this.from = inputDate;
  }
  
  void setTill(Date inputDate){
    this.till = inputDate;
  }

  Date getFrom(){
    return this.from;
  }

  Date getTill(){
    return this.till;
  }
  
  int getDaysBetween(){
    int between = (int)((this.getTill().getTime()-this.getFrom().getTime())/(1000 * 60 * 60 * 24));
    return between + 1;
  }
  
  Date stringWildCard(String inputDateTime) throws ParseException{
      Date output = null;
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
      SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
      try {
        output = dateFormat.parse(inputDateTime);
      } catch (ParseException e){
      output = timeFormat.parse(inputDateTime);
      }
      return output;
  }
  
  Date stringDate(String inputDate){
      Date output = null;
      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
      try {
        output = format.parse(inputDate);
      } catch (ParseException e){
        println(e);
      }
      return output;
  }
  
  Date stringTime(String inputTime){
      Date output = null;
      SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
      try {
        output = format.parse(inputTime);
      } catch (ParseException e){
        println(e);
      }
      return output;
  }
  
  Date stringDateTime(String inputDateTime){
      Date output = null;
      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy-HH:mm:ss");
      try {
        output = format.parse(inputDateTime);
      } catch (ParseException e){
        println(e);
      }
      return output;
  }

}
