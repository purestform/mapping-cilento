// Use Subdivision Class
import java.util.*;
import java.awt.Rectangle;
import geotools.subdivision.*;

MongoDB m;
String ipAddress       = "127.0.0.1";
String targetDB        = "xxxx";
String targetTweets    = "xxxx";
String targetPeople    = "xxxx";
String username        = "xxxx";
String password        = "xxxx";



Location mapCen;
DraggableMap map;




PGraphics canvas;
PImage primary;
PImage secondary;

// Rendering
boolean isGrid        = true;
boolean isPoints;
boolean isTrips       = true;
boolean isArc         = true;
boolean isInterface   = true;
boolean isCilento     = false;
boolean isCellCol     = false;
boolean isDays        = true;
boolean isFaded;
boolean isFilterNodes = true;
boolean isEdDir       = true;
boolean isOutEd       = true;
boolean isInEd        = true;

// Interaction 
boolean isDrag;
boolean isWheel;
boolean isHist;
boolean isDraw;
boolean isMap       = true;
boolean isEqual     = true;

// Italy Zoom Level 1
float mapCenX = 41.288;
float mapCenY = 14.116;
float zoomSC  = 61.671284;
int zoom = 6;

// Italy Zoom Level 2
float mapCen2X = 40.273;
float mapCen2Y = 15.399;
float zoomSC2 = 641.45844;
int zoom2 = 9;

//// Italy Zoom Level 3
//float mapCen3X = 40.346;
//float mapCen3Y = 15.271;
//float zoomSC3 = 859.61554;
//int zoom3 = 10;

// Italy Zoom Level 3
float mapCen3X = 40.255;
float mapCen3Y = 15.613;
float zoomSC3 = 859.61554;
int zoom3 = 10;

// Foursqare Data Points
List<Location> foursquare;

// Individuals
int minTweets = 2;
int maxTweets = 2000;
int maxPeople = 4000;
List<Object> ids;
Map<Object, Individual> individuals;

// Nodes
int maxActiveDays;
int minActiveDays;
int minPeopleCells;
int maxPeopleCells;
Map<Location, Integer> daysActiveCells;
Map<Location, Integer> peopleActiveCells;
Map<Location, Map<Object, Set<Entry>>> clusters;
Map<Cell, Map<Object, List<Entry>>> entriesInCells;
Map<Location, Integer> uniqueID;

// Edges
int[] ppleRange;
int[] tripRange;
int[] distRange;
int[] inEdRange;
int[] ouEdRange;
ScaleMap ppleScale;
ScaleMap tripScale;
ScaleMap inEdScale;
ScaleMap ouEdScale;
Map<Object, Set<Edge>> edges;
Map<Location, Set<Edge>> outgoing;
Map<Location, Set<Edge>> incoming;

// Store Edge Selection
Set<Edge> incomingEdges;
Set<Edge> outgoingEdges;
Set<Location> selection;

// Histogram
int nBins = 17;
Histogram tripFilter;
Histogram ppleFilter;
Histogram inEdFilter;
Histogram ouEdFilter;
Histogram distFilter;
Histogram timeFilter;
int minWtPple;
int maxWtPple;
int minWtTrip;
int maxWtTrip;
int minWtDist;
int maxWtDist;
int minWtInEd;
int maxWtInEd;
int minWtOuEd;
int maxWtOuEd;

// Timeline
Date frDate;
Date toDate;
DateFormat inFormat;
DateFormat outFormat;
TimeLine timeline;

// Colours
int colIncoming;
int colOutgoing;
int colNeutral;

// Grid
int canvasWidth = 1300;
int canvasHeight = 800;
int gridDim = 256;
int maxSubdivision = 9;
int minSubdivision = 3;
int curSubdivision = 7;

Map<Location, Location[]> gridLoc;
Map<Location, PVector[]>  gridPos;
Map<Location, Float> gridWid;

Location gridCen;
PVector centre;


void setup() {

  // Setup PApplet
  size(canvasWidth, canvasHeight);
  rectMode(CENTER);
  noFill();

  frame.setTitle("FlowSampler");

  // Setup DB
  m = new MongoDB(ipAddress, targetDB, username, password);

  // Setup Map
  mapCen = new Location(mapCenX, mapCenY);
  map = new DraggableMap(this, mapCen, zoom);
  map.setZoomSC(zoomSC);

  // Setup Graphic Buffer & Canvas
  canvas = createGraphics(width, height);
  primary = canvas.get();

  // ****** Begin Load From Server
  // Query Foursquare Data
  foursquare = new ArrayList();
  m.setCollection("foursquare");  
  DBCursor fsCursor = m.getData();
  while(fsCursor.hasNext()){
    DBObject curObj = (DBObject) fsCursor.next();
    // Get Location
    DBObject comObj = (DBObject) curObj.get("loc");
    Object oLat = (Object) comObj.get("lat");
    Object oLon = (Object) comObj.get("lon");
    float lat = 0;
    float lon = 0;
    if(oLat instanceof Integer){
      lat = (Integer) comObj.get("lat");
    } else {
      double d = (Double) comObj.get("lat");
      lat = (float) d;
    }
    if(oLon instanceof Integer){
      lon = (Integer) comObj.get("lon");
    } else {
      double d = (Double) comObj.get("lon");
      lon = (float) d;
    }
    Location loc = new Location(lat,lon);   
    foursquare.add(loc); 
  }
  
  // Query List of Individuals
  m.setCollection(targetPeople);
  
  DBObject isDesc = new BasicDBObject();
  isDesc.put("description", new BasicDBObject("$exists", true));

  DBObject isTimezone = new BasicDBObject();
  DBObject noTimezone = new BasicDBObject();
  isTimezone.put("timezone", new BasicDBObject("$exists", true));
  noTimezone.put("timezone", new BasicDBObject("$exists", false));

  DBObject isAmsterdam = new BasicDBObject("timezone", "Amsterdam");
  DBObject isAthens    = new BasicDBObject("timezone", "Athens");
  DBObject isGreenland = new BasicDBObject("timezone", "Greenland");
  
  
  DBObject isHawaii = new BasicDBObject("timezone", "Hawaii");
  DBObject isEastUS = new BasicDBObject("timezone", "Eastern Time (US & Canada)");
  DBObject isPaciUS = new BasicDBObject("timezone", "Pacific Time (US & Canada)");
  DBObject isCentUS = new BasicDBObject("timezone", "Central Time (US & Canada)");
  DBObject isAtlant = new BasicDBObject("timezone", "Atlantic Time (Canada)");
  DBObject isArizon = new BasicDBObject("timezone", "Arizona");
  DBObject isMount  = new BasicDBObject("timezone", "Mountain Time (US & Canada)");
  BasicDBList orList2 = new BasicDBList();
  orList2.put(orList2.size(), isHawaii);
  orList2.put(orList2.size(), isEastUS);
  orList2.put(orList2.size(), isPaciUS);
  orList2.put(orList2.size(), isCentUS);
  orList2.put(orList2.size(), isAtlant);
  orList2.put(orList2.size(), isArizon);
  orList2.put(orList2.size(), isMount);
  DBObject isNAmeric = new BasicDBObject("$or", orList2);
  
  

 
  
  DBObject noRome = new BasicDBObject();
  DBObject noEURome = new BasicDBObject();
  DBObject isRome = new BasicDBObject();
  DBObject isEURome = new BasicDBObject();
  noRome.put("timezone", new BasicDBObject("$ne", "Rome"));
  noEURome.put("timezone", new BasicDBObject("$ne", "Europe/Rome"));
  isRome.put("timezone", "Rome");
  isEURome.put("timezone", "Europe/Rome");
  
  DBObject tweetCond = new BasicDBObject();
  tweetCond.put("nDaysPercent", new BasicDBObject("$lt",75));
  
  // ******* Get visitors
  BasicDBList andList1 = new BasicDBList();
  BasicDBList orListForeign = new BasicDBList();
  orListForeign.put(orListForeign.size(), isGreenland);
  orListForeign.put(orListForeign.size(), isNAmeric);
  orListForeign.put(orListForeign.size(), isAmsterdam);
  orListForeign.put(orListForeign.size(), isAthens);
  andList1.put(andList1.size(), new BasicDBObject("$or", orListForeign));
  andList1.put(andList1.size(), isTimezone);
  DBObject foreignIsTimezone = new BasicDBObject("$and", andList1);
  // ******* Get all visitors from foreign timezones

  // ******* Get all visitors with no timezone
  BasicDBList andList2 = new BasicDBList();
  andList2.put(andList2.size(), tweetCond);
  andList2.put(andList2.size(), noTimezone);
  DBObject andQuery2 = new BasicDBObject("$and", andList2);
  // ******* Get all visitors with no timezone
  



  // ******* Get all Domestic tourist
  // Get all visitors with timezone Rome
  BasicDBList orList1 = new BasicDBList();
  orList1.put(orList1.size(), isRome);
  orList1.put(orList1.size(), isEURome);
  DBObject orQuery = new BasicDBObject("$or", orList1);
  BasicDBList andList3 = new BasicDBList();
  andList3.put(andList3.size(), orQuery);
  andList3.put(andList3.size(), isTimezone);
  andList3.put(andList3.size(), new BasicDBObject("local", new BasicDBObject("$exists", false)));
  DBObject italianIsTimezone = new BasicDBObject("$and", andList3);

  // Get all likely domestic (no timezone) visitors
  BasicDBList andList6 = new BasicDBList();
  andList6.put(andList6.size(), new BasicDBObject("nDaysActive", new BasicDBObject("$gte",14)));
  andList6.put(andList6.size(), noTimezone);
  andList6.put(andList6.size(), new BasicDBObject("local", new BasicDBObject("$exists", false)));
  DBObject italianNoTimezone = new BasicDBObject("$and", andList6);
  
  
  // ******* Get Locals
  BasicDBList orList4 = new BasicDBList();
  orList4.put(orList4.size(), isRome);
  orList4.put(orList4.size(), isEURome);
  DBObject orQuery2 = new BasicDBObject("$or", orList1);
  BasicDBList andList4 = new BasicDBList();
  andList4.put(andList4.size(), orQuery2);
  andList4.put(andList4.size(), isTimezone);
  andList4.put(andList4.size(), new BasicDBObject("nDaysActiveCilento", new BasicDBObject("$gte",3)));
  andList4.put(andList4.size(), new BasicDBObject("nDaysPercent", new BasicDBObject("$gte",75)));
  DBObject localsNoTimezone = new BasicDBObject("$and", andList4);
  
  BasicDBList andList5 = new BasicDBList();
  andList5.put(andList5.size(), new BasicDBObject("nDaysActiveCilento", new BasicDBObject("$gte",3)));
  andList5.put(andList5.size(), new BasicDBObject("nDaysActive", new BasicDBObject("$gte",14)));
  andList5.put(andList5.size(), new BasicDBObject("nDaysPercent", new BasicDBObject("$gte",75)));
  andList5.put(andList5.size(), noTimezone);
  DBObject localsIsTimezone = new BasicDBObject("$and", andList5);
    
  BasicDBList orListFinal = new BasicDBList();
  orListFinal.put(orListFinal.size(), italianIsTimezone);
  orListFinal.put(orListFinal.size(), italianNoTimezone);
  orListFinal.put(orListFinal.size(), foreignIsTimezone);
  orListFinal.put(orListFinal.size(), localsNoTimezone);
  orListFinal.put(orListFinal.size(), localsIsTimezone);

  DBObject queryFinal = new BasicDBObject("$or", orListFinal);
  println(queryFinal);
  
  DBCursor idCursor = m.getData(queryFinal);
  println(idCursor.size());

  // Store individuals
  individuals = new HashMap();

  // Get List of Individual Ids
  ids = new ArrayList();
  while (idCursor.hasNext ()) {
    if (ids.size() > (maxPeople-1)) break;
    DBObject dbObj = (DBObject) idCursor.next();
    Object id = (Object) dbObj.get("user_id");
    ids.add(id);
  }
  println("Num of Individuals: " + ids.size());
  // ****** End Load From Server


  // Get Tweets for Individuals
  m.setCollection(targetTweets);
  for (Object obj : ids) {
    Individual ind = new Individual();

    // Get data for individuals
    DBObject userId = new BasicDBObject();
    userId.put("from_user_id", obj);
    userId.put("cliento", new BasicDBObject("$exists", true));

    DBCursor cursor = m.getData(userId);
    List<Entry> indEntries = getEntries(cursor, map);
    for (Entry entry : indEntries) {
      ind.add(entry);
    }

    individuals.put(obj, ind);
    println("Loaded Id: " + obj + " - " + cursor.size());
  }


  // ***** Sort by quantity
  Map<Object, Integer> sortByQuantity = new HashMap();
  for (Object obj : ids) {
    Individual ind = (Individual) individuals.get(obj);
    sortByQuantity.put(obj, ind.entries.size());
  }
  sortByQuantity = sortByValue(sortByQuantity, false);
  ids = new ArrayList(sortByQuantity.keySet());
  Collections.reverse(ids);

  colIncoming = color(215, 48, 39);
  colOutgoing = color(69, 117, 180);
  colNeutral  = color(40, 40, 40);

  // **** Create Grid
  initGrid();
  println("Num Clusters: " + clusters.size());
  println("Num Edges: " + edges.size());

  // **** Render Canvas
  renderPrimary();
}


void renderMapTint(int inValue) {
  noStroke();
  fill(0, inValue);
  rect(width/2, height/2, width, height);
}


void renderPrimary() {

  canvas.beginDraw();
  canvas.background(0, 0, 0, 0);
  canvas.stroke(0);
  
  if (isGrid) {
    

    // Render grid
    canvas.stroke(0, 0, 0, 30);
    for (Location locCN : gridPos.keySet ()) {
      int uid = (Integer) uniqueID.get(locCN);
      int count = 0;
      int brightness = 30;
      canvas.noFill();
      if(isCellCol){
        if(isDays){
          if(daysActiveCells.containsKey(locCN)){
            count = (Integer) daysActiveCells.get(locCN);
            brightness = (int) map(count, minActiveDays, maxActiveDays, 30, 255);
            canvas.fill(255,0,0,brightness);
          }
        } else {
          if(peopleActiveCells.containsKey(locCN)){
            count = (Integer) peopleActiveCells.get(locCN);
            brightness = (int) map(count, minPeopleCells, maxPeopleCells, 30, 255);
            canvas.fill(255,0,0,brightness);
          }
        }
      }
      PVector[] geometry = (PVector[]) gridPos.get(locCN);
      canvas.beginShape();
      canvas.vertex(geometry[0].x, geometry[0].y);
      canvas.vertex(geometry[1].x, geometry[1].y);
      canvas.vertex(geometry[2].x, geometry[2].y);
      canvas.vertex(geometry[3].x, geometry[3].y);
      canvas.endShape(CLOSE);
    }
    
  }


  if (!isPoints) {
    
    Set<Location> toDrawNodes = new HashSet();
    if(isFilterNodes){
      for (Location locCN : gridPos.keySet ()) {
        if(incoming.containsKey(locCN) && outgoing.containsKey(locCN)){
          Set<Edge> incomingEdges = (HashSet) incoming.get(locCN);
          Set<Edge> outgoingEdges = (HashSet) outgoing.get(locCN);
          int inEd = (int) inEdScale.getMappedValueFor(log(incomingEdges.size()));
          int ouEd = (int) ouEdScale.getMappedValueFor(log(outgoingEdges.size()));
          if(inEd >= minWtInEd && inEd <= maxWtInEd){
            if(ouEd >= minWtOuEd && ouEd <= maxWtOuEd){
              toDrawNodes.add(locCN);
            }
          }
        }
      }
    } else {
      for (Location locCN : gridPos.keySet ()) {
        toDrawNodes.add(locCN);
      }
    }
    
    // Render edges with variable weighting
    // Draw edge only once
    Set<Edge> toDraw = new HashSet();
    for (Object id : edges.keySet ()) {
      Set<Edge> indEdges = (HashSet) edges.get(id);
      Iterator it = indEdges.iterator();
      boolean isSelect = false;
      while (it.hasNext ()) {
        Edge e = (Edge) it.next();
        
        if(toDrawNodes.contains(e.from) && toDrawNodes.contains(e.to)){
          int nPple     = e.getNumIndividuals();
          int nTrips    = e.getNumEntries();
          int nDist     = e.getDistance();
          int logPple   = (int) ppleScale.getMappedValueFor(log(nPple));
          int logTrips  = (int) tripScale.getMappedValueFor(log(nTrips));
          int linDist   = (int) map(nDist, distRange[0], distRange[1], 0, nBins);
          
          boolean isInTimeline = false;
          for (Entry[] entries : e.getEntries ()) {
            Date d1 = entries[0].date;
            Date d2 = entries[1].date;
            if (d1.after(frDate) && d1.before(toDate) ||
               (d2.after(frDate) && d2.before(toDate))) {
              isInTimeline = true;
              break;
            }
          }
          
          if (logPple >= minWtPple && logPple <= maxWtPple) {
            if (logTrips >= minWtTrip && logTrips <= maxWtTrip) {
              if (linDist >= minWtDist && linDist <= maxWtDist) {
                if (isInTimeline) {
                  if(isCilento){
                    // ***** arbitartily select incoming and 
                    // ***** outgoing edges directed at cilento                  
                    for (Entry[] entries : e.getEntries ()) {
                      if (entries[0].isFlag && entries[1].isFlag) {
                        toDraw.add(e);
                        break;
                      }
                    }
                    // ***** arbitartily select incoming and
                    // ***** outgoing edges directed at cilento
                  } else {
                    toDraw.add(e);
                  }
                }
              }
            }
          }
        }
        
      }
    }
    

    if (incomingEdges != null) {
      Iterator incomingEdgesIt = incomingEdges.iterator();
      while (incomingEdgesIt.hasNext ()) {
        Edge e = (Edge) incomingEdgesIt.next();
        if (toDraw.contains(e)) {
          toDraw.remove(e);
        } else {
          incomingEdgesIt.remove();
        }
      }
    }

    if (outgoingEdges != null) {
      Iterator outgoingEdgesIt = outgoingEdges.iterator();
      while (outgoingEdgesIt.hasNext ()) {
        Edge e = (Edge) outgoingEdgesIt.next();
        if (toDraw.contains(e)) {
          toDraw.remove(e);
        } else {
          outgoingEdgesIt.remove();
        }
      }
    }
    
    
    boolean isDim = false; // Dim other edges if a node cluster is selected
    if (outgoingEdges.size() > 0 || incomingEdges.size() > 0) {
      isDim = true;
    }

    int opacityScalar = 1;
    if (isDim) opacityScalar = 4;

    println("To Draw: " + toDraw.size());

    
    if(incomingEdges.size() > 1 || outgoingEdges.size() > 1){
      isFaded = true;
    } else {
      isFaded = false;
    }    
    
    

    
    // Render
    Iterator toDrawIt = toDraw.iterator();
    while (toDrawIt.hasNext ()) {
      Edge e = (Edge) toDrawIt.next();
      int nPple    = e.getNumIndividuals();
      int nTrips   = e.getNumEntries();
      int logPple  = (int) ppleScale.getMappedValueFor(log(nPple));
      int logTrips = (int) tripScale.getMappedValueFor(log(nTrips));
      PVector p1   = map.locationPoint(e.from);
      PVector p2   = map.locationPoint(e.to);
      
      if (p1.x != p2.x || p1.y != p2.y) {
        int col = color(colNeutral, 80);
        if(isFaded) col = color(colNeutral, 20);
        if (isArc) {
          if (isTrips) renderArc(canvas, p1, p2, col, opacityScalar, logTrips);
          else renderArc(canvas, p1, p2, col, opacityScalar, logPple);
        } else {
          if (isTrips) renderTLine(canvas, p1, p2, col, logTrips, 1);
          else renderTLine(canvas, p1, p2, col, logPple, 1);
        }
      } else {
        if(map.getZoom() < 12){
          int col = color(colNeutral, 160);
          if(isFaded) col = color(colNeutral, 40);
          canvas.stroke(col);
          if (isTrips) canvas.strokeWeight(logTrips);
          else canvas.strokeWeight(logPple);
          canvas.point(p1.x, p1.y);
          canvas.strokeWeight(1);
        } else {
          int col = color(colNeutral, 160);
          float cDim = (Float) gridWid.get(e.from);
          float cWidth = cDim/4;
          PVector e1 = new PVector(p1.x + cWidth, p1.y);
          PVector e2 = new PVector(p1.x - cWidth, p1.y);    
          int wt = 0;
          if (isTrips) wt = logTrips;
          else wt = logPple;
          renderArc(canvas, e1, e2, col, opacityScalar, wt);
          renderArc(canvas, e2, e1, col, opacityScalar, wt);
        }
      }
    }


    Iterator incomingEdgesIt = incomingEdges.iterator();
    while (incomingEdgesIt.hasNext ()) {
      Edge e = (Edge) incomingEdgesIt.next();
      int nPple    = e.getNumIndividuals();
      int nTrips   = e.getNumEntries();
      int logPple   = (int) ppleScale.getMappedValueFor(log(nPple));
      int logTrips  = (int) tripScale.getMappedValueFor(log(nTrips));
      PVector p1 = map.locationPoint(e.from);
      PVector p2 = map.locationPoint(e.to);
      if (p1.x != p2.x || p1.y != p2.y) {
        int col = colIncoming;
        if (isArc) {
          if (isTrips) renderArc(canvas, p1, p2, col, opacityScalar, logTrips);
          else renderArc(canvas, p1, p2, col, opacityScalar, logPple);
        } else {
          if (isTrips) renderTLine(canvas, p1, p2, col, logTrips, 1);
          else renderTLine(canvas, p1, p2, col, logPple, 1);
        }
      } else {
        if(map.getZoom() < 12){
          int col = color(colNeutral, 160);
          canvas.stroke(col);
          if (isTrips) canvas.strokeWeight(logTrips);
          else canvas.strokeWeight(logPple);
          canvas.point(p1.x, p1.y);
          canvas.strokeWeight(1);
        } else {
          float cDim = (Float) gridWid.get(e.to);
          float cWidth = cDim/4;
          PVector e1 = new PVector(p1.x + cWidth, p1.y);
          PVector e2 = new PVector(p1.x - cWidth, p1.y);
          int wt = 0;
          if (isTrips) wt = logTrips;
          else wt = logPple;
          renderArc(canvas, e1, e2, colIncoming, opacityScalar, wt);
          renderArc(canvas, e2, e1, colOutgoing, opacityScalar, wt);
        }
      }
    }


    Iterator outgoingEdgesIt = outgoingEdges.iterator();
    while (outgoingEdgesIt.hasNext ()) {
      Edge e = (Edge) outgoingEdgesIt.next();
      int nPple    = e.getNumIndividuals();
      int nTrips   = e.getNumEntries();
      int logPple   = (int) ppleScale.getMappedValueFor(log(nPple));
      int logTrips  = (int) tripScale.getMappedValueFor(log(nTrips));
      PVector p1 = map.locationPoint(e.from);
      PVector p2 = map.locationPoint(e.to);
      if (p1.x != p2.x || p1.y != p2.y) {
        int col = colOutgoing;    
        if (isArc) {
          if (isTrips) renderArc(canvas, p1, p2, col, opacityScalar, logTrips);
          else renderArc(canvas, p1, p2, col, opacityScalar, logPple);
        } else {
          if (isTrips) renderTLine(canvas, p1, p2, col, logTrips, 1);
          else renderTLine(canvas, p1, p2, col, logPple, 1);
        }
      } else {
        if(map.getZoom() < 12){
          int col = color(colNeutral, 160);    
          canvas.stroke(col);
          if (isTrips) canvas.strokeWeight(logTrips);
          else canvas.strokeWeight(logPple);
          canvas.point(p1.x, p1.y);
          canvas.strokeWeight(1);
        } else {
          float cDim = (Float) gridWid.get(e.from);
          float cWidth = cDim/4;
          PVector e1 = new PVector(p1.x + cWidth, p1.y);
          PVector e2 = new PVector(p1.x - cWidth, p1.y);
          int wt = 0;
          if (isTrips) wt = logTrips;
          else wt = logPple;
          renderArc(canvas, e1, e2, colIncoming, opacityScalar, wt);
          renderArc(canvas, e2, e1, colOutgoing, opacityScalar, wt);
        }
      }
    }
    
  } else {

    canvas.strokeWeight(3);
    for (Cell c : entriesInCells.keySet ()) {
      Location cellCentroid = map.pointLocation(c.centre);
      Set<Entry> cellEntries = new HashSet();
      Map<Object, List<Entry>> individualsInCell = (HashMap) entriesInCells.get(c);
      for (Object id : individualsInCell.keySet ()) {
        List<Entry> entries = (ArrayList) individualsInCell.get(id);
        for (Entry e : entries) {
          e.position = map.locationPoint(e.location);
          //if(e.isFlag) canvas.stroke(25, 197, 204, 80);
          //else canvas.stroke(185,0,0,80);
          canvas.stroke(185,0,0,80);
          canvas.point(e.position.x, e.position.y);
        }
      }
    }
    canvas.strokeWeight(1);
    
  }

  canvas.endDraw();

  primary = canvas.get();
  isDraw = false;
}



void draw() {

  if (map.getWheelStatus() == 0) isWheel = false;
  else isWheel = true;    

  if (!isWheel) {
    if (isDraw) {
      updateGridPos();
      renderPrimary();
    }
  }

  background(255);
  map.draw();

  if (!isDrag && !isWheel) {
    image(primary, 0, 0);
  }

  if(isInterface){
    if (tripFilter != null &&
        ppleFilter != null &&
        distFilter != null &&
        inEdFilter != null &&
        ouEdFilter != null &&
        timeline != null) {
      rectMode(CORNER);
      tripFilter.display();
      ppleFilter.display();
      distFilter.display();
      inEdFilter.display();
      ouEdFilter.display();
      timeline.display();
      rectMode(CENTER);
    }
  }

  if (map.isSleep()) {
    noLoop();
  }
  
}



Map<Cell, Map<Object, List<Entry>>> getEntriesInCells(Map<Object, Individual> inIndividuals, Grid inEQ) {

  // Output
  Map<Cell, Map<Object, List<Entry>>> output = new HashMap();

  // Reset Bins
  Map<Object, boolean[]> trackBinned = new HashMap();
  for (Object id : inIndividuals.keySet ()) {
    Individual ind = (Individual) inIndividuals.get(id);
    boolean[] binned = new boolean[ind.entries.size()];
    trackBinned.put(id, binned);
  }

  // Count Points In Cell
  int count = 0;
  for (Cell c : inEQ.grid) {
    Map<Object, List<Entry>> individualsInCell = new HashMap();
    for (Object id : inIndividuals.keySet ()) {
      Individual ind = (Individual) inIndividuals.get(id);
      boolean[] binned = (boolean[]) trackBinned.get(id);
      List<Entry> entries = ind.getEntries();
      List<Entry> entriesInCell = new ArrayList();
      for (int i = 0; i < entries.size (); i++) {
        Entry entry = (Entry) entries.get(i);
        if (!binned[i]) {
          boolean res = c.contains(entry.position);
          if (res) {
            entriesInCell.add(entry);
            binned[i] = true;
          }
        }
      }
      if (entriesInCell.size() > 0) {
        individualsInCell.put(id, entriesInCell);
      }
    }
    if (individualsInCell.size() > 0) count++;
    output.put(c, individualsInCell);
  }

  println("Num Cells with Data: " + count + "/" + output.size());
  return output;
}




Map sortByValue(Map unsortMap, boolean isReverse) {
  List list = new LinkedList(unsortMap.entrySet());
  Collections.sort(list, new Comparator() {
    public int compare(Object o1, Object o2) {
      return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
    }
  }
  );
  if (isReverse) Collections.reverse(list);
  Map sortedMap = new LinkedHashMap();
  for (Iterator it = list.iterator (); it.hasNext(); ) {
    Map.Entry entry = (Map.Entry) it.next();
    sortedMap.put(entry.getKey(), entry.getValue());
  }
  return sortedMap;
}



void createInternalGrid(Grid inEQ, int inMaxRecur) {

  Map<Cell, Integer> subdivision = new HashMap();
  for (int i=0; i<inEQ.grid.size (); i++) {
    Cell c = (Cell) inEQ.grid.get(i);
    if (c.count > 0) {
      int nRec = ceil(quadRoot(c.count));
      int tRec = nRec + c.level;
      if (tRec > inMaxRecur) {
        nRec = inMaxRecur - c.level;
      }
      if (nRec > 0) {
        subdivision.put(c, nRec);
      }
    }
  }

  if (isEqual) {
    inEQ = (EqualGrid) inEQ;
    Map<Cell, List<Cell>> internalCells = inEQ.breakDown(subdivision);
  } else {
    inEQ = (QuadTree) inEQ;
    Map<Cell, List<Cell>> internalCells = inEQ.breakDown(subdivision);
  }

  println();
  println("** Subdivision **");
}


void updateGridPos() {
  for (Location locCN : gridLoc.keySet ()) {
    Location[] geometry = (Location[]) gridLoc.get(locCN);
    PVector cellTL = map.locationPoint(geometry[0]);
    PVector cellBR = map.locationPoint(geometry[1]);
    PVector cellTR = new PVector(cellBR.x, cellTL.y);
    PVector cellBL = new PVector(cellTL.x, cellBR.y);
    PVector[] cellPos = {
      cellTL, cellTR, cellBR, cellBL
    };
    float cellWidth = abs(cellTL.x - cellTR.x);
    gridPos.put(locCN, cellPos);
    gridWid.put(locCN, cellWidth);
  }
}





float quadRoot(int input) {
  if (input > 0) {
    return pow(input, .25);
  }
  return 0;
}


int arrayMinIndex(float[] inArray) {
  int index = 0;
  for (int i=0; i<inArray.length; i++) {
    if (inArray[i] < inArray[index]) {
      index = i;
    }
  }
  return index;
}

static int[] getExtrema(Set<Integer> inSet) {
  if (inSet.size() > 0) {
    int min = Collections.min(inSet);
    int max = Collections.max(inSet);
    if (min == 0) min = 1;
    return new int[] {
      min, max
    };
  } else return null;
}

void addToMap(Map<Integer, Integer> inMap, int inKey) {
  if (inMap.containsKey(inKey)) {
    int val = (Integer) inMap.get(inKey);
    inMap.put(inKey, val + 1);
  } else {
    inMap.put(inKey, 1);
  }
}

void addToMap(Map<String, Integer> inMap, String inKey) {
  if (inMap.containsKey(inKey)) {
    int val = (Integer) inMap.get(inKey);
    inMap.put(inKey, val + 1);
  } else {
    inMap.put(inKey, 1);
  }
}

void addToMap(Map<Integer, Set<Integer>> inMap, Integer inKey, Integer inVal) {
  if (inMap.containsKey(inKey)) {
    Set<Integer> val = (HashSet) inMap.get(inKey);
    val.add(inVal);
  } else {
    Set<Integer> val = new HashSet();
    val.add(inVal);
    inMap.put(inKey, val);
  }
}

void addToMap(Map<String, Set<Object>> inMap, String inKey, Object inId) {
  if (inMap.containsKey(inKey)) {
    Set<Object> val = (HashSet) inMap.get(inKey);
    val.add(inId);
  } else {
    Set<Object> val = new HashSet();
    val.add(inId);
    inMap.put(inKey, val);
  }
}

void addToMap(Map<Location, Set<Edge>> inMap, Location inKey, Edge inEdge) {
  if (inMap.containsKey(inKey)) {
    Set<Edge> val = (HashSet) inMap.get(inKey);
    val.add(inEdge);
  } else {
    Set<Edge> val = new HashSet();
    val.add(inEdge);
    inMap.put(inKey, val);
  }
}

static int getDayOfYear(Date inputDate) {
  Calendar cal = Calendar.getInstance();
  cal.setTime(inputDate);
  return(cal.get(Calendar.DAY_OF_YEAR));
}

static int getDateInMillis(Date inputDate) {
  Calendar cal = Calendar.getInstance();
  cal.setTime(inputDate);
  return(cal.get(Calendar.DAY_OF_YEAR));
}

