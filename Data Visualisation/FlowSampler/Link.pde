void renderArc(PGraphics inTarget, PVector startLoc, PVector endLoc, int inColor, int inOpacity, int weight) {

  float wtStep = weight * .075;
  PVector vel = PVector.sub(startLoc, endLoc);
  float theta = vel.heading() + radians(90);
  float len = startLoc.dist(endLoc)/2;
  float mx = map(0.5, 0, 1, startLoc.x, endLoc.x);
  float my = map(0.5, 0, 1, startLoc.y, endLoc.y);
  PVector r = new PVector(mx, my);

  float start = radians(90);
  float stop = radians(270);
  float segment = PI/32;
  float ri = len;
  float ro = len;

  float opacity = 0;
  float opacityStep = (255/(stop - start))/2;

  inTarget.noStroke();
  inTarget.pushMatrix();
  inTarget.translate(r.x, r.y);
  inTarget.rotate(theta + radians(90));
  inTarget.beginShape(TRIANGLE_STRIP);  
  for (float i=start; i<stop; i+=segment) {
    inTarget.vertex(ri * sin(i), ri * cos(i));
    inTarget.vertex(ro * sin(i), ro * cos(i));
    inTarget.fill(inColor);
    //inTarget.fill(inColor, opacity / inOpacity);
    ri += wtStep/4;
    ro -= wtStep/4;
    opacity += opacityStep;
  }
  inTarget.endShape();  
  inTarget.popMatrix();
  
}


void renderArc(PGraphics inTarget, PVector startLoc, PVector endLoc, boolean modifier, int weight) {

  float wtStep = weight * .075;
  PVector vel = PVector.sub(startLoc, endLoc);
  float theta = vel.heading() + radians(90);
  float len = startLoc.dist(endLoc)/2;
  float mx = map(0.5, 0, 1, startLoc.x, endLoc.x);
  float my = map(0.5, 0, 1, startLoc.y, endLoc.y);
  PVector r = new PVector(mx, my);

  float start = radians(90);
  float stop = radians(270);
  float segment = PI/32;
  float ri = len;
  float ro = len;

  float opacity = 0;
  float opacityStep = (255/(stop - start))/20;

  inTarget.noStroke();
  inTarget.pushMatrix();
  inTarget.translate(r.x, r.y);
  inTarget.rotate(theta + radians(90));
  inTarget.beginShape(TRIANGLE_STRIP);  
  for (float i=start; i<stop; i+=segment) {
    inTarget.vertex(ri * sin(i), ri * cos(i));
    inTarget.vertex(ro * sin(i), ro * cos(i));
    inTarget.fill(255);
    //inTarget.fill(255, 255, 255, opacity/4);
    //if (modifier) inTarget.fill(255, 0, 0, opacity);
    ri += wtStep/4;
    ro -= wtStep/4;
    opacity += opacityStep;
  }
  inTarget.endShape();  
  inTarget.popMatrix();
  
}



void renderTLine(PGraphics inTarget, PVector startLoc, PVector endLoc, int inColor, float arrowsize, float scayl) {
  PVector diff = PVector.sub(endLoc, startLoc);
  float len = diff.mag() * scayl;
  float arr = (arrowsize)*.75;
  inTarget.noStroke();
  inTarget.pushMatrix();
  inTarget.translate(startLoc.x, startLoc.y);
  inTarget.rotate(diff.heading2D());
  inTarget.beginShape(TRIANGLES);
  inTarget.fill(inColor);
  inTarget.vertex(len, 0);
  inTarget.fill(inColor);
  inTarget.vertex(0, arr);
  inTarget.vertex(0, -arr);
  inTarget.endShape(CLOSE);
  inTarget.popMatrix();
  inTarget.noFill();
}
