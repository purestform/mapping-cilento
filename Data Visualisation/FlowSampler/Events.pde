void mouseMoved() {
  
  if(tripFilter != null && ppleFilter != null && timeline != null){
    cursor(ARROW);
    isHist = false;
    if (tripFilter.hover_area.contains(mouseX, mouseY)) {
      cursor(HAND);
      isHist = true;
    } else if (ppleFilter.hover_area.contains(mouseX, mouseY)) {
      cursor(HAND);
      isHist = true;
    } else if (distFilter.hover_area.contains(mouseX, mouseY)) {
      cursor(HAND);
      isHist = true;
    } else if (inEdFilter.hover_area.contains(mouseX, mouseY)) {
      cursor(HAND);
      isHist = true;
    } else if (ouEdFilter.hover_area.contains(mouseX, mouseY)) {
      cursor(HAND);
      isHist = true;
    } else if (timeline.hover_area.contains(mouseX, mouseY)) {
      cursor(HAND);
      isHist = true;
    }
  }
  loop();
  
}


void mouseDragged() {
  if (isHist) {
    if(tripFilter != null){
      tripFilter.onDrag(mouseX);
    }
    if(ppleFilter != null){
      ppleFilter.onDrag(mouseX);
    }
    if(distFilter != null){
      distFilter.onDrag(mouseX);
    }
    if(inEdFilter != null){
      inEdFilter.onDrag(mouseX);
    }
    if(ouEdFilter != null){
      ouEdFilter.onDrag(mouseX);
    } 
    if(timeline != null){
      timeline.onDrag(mouseX);
    }
  } else {
    isDrag = true;    
    map.mouseDragged();
  }
  loop();
}


void mouseReleased(){
  
  if (isHist) {
    println("[Replot] - MouseDrag Histogram");
    updateTripFilterRange();
    updatePpleFilterRange();
    updateDistFilterRange();
    updateInEdFilterRange();
    updateOuEdFilterRange();
    updateTimelineRange();
    isHist = false;
    isDraw = true;
  }
  
  if(isDrag){
    println("[Replot] - MouseDrag Map");
    map.mouseReleased();
    isDraw = true;
    isDrag = false;
  }
      
  loop();

}


void mousePressed() {
  if(tripFilter != null &&
     ppleFilter != null &&
     distFilter != null &&
     inEdFilter != null &&
     ouEdFilter != null &&
     timeline != null){
    tripFilter.resetFlags(); 
    ppleFilter.resetFlags();
    distFilter.resetFlags();
    inEdFilter.resetFlags();
    ouEdFilter.resetFlags();
    timeline.resetFlags(); 
    if (isHist) {
      if (tripFilter.hover_area.contains(mouseX, mouseY)) {
        tripFilter.onPressed(mouseX);
      } else if (ppleFilter.hover_area.contains(mouseX, mouseY)) {
        ppleFilter.onPressed(mouseX);
      } else if (distFilter.hover_area.contains(mouseX, mouseY)) {
        distFilter.onPressed(mouseX);
      } else if (inEdFilter.hover_area.contains(mouseX, mouseY)) {
        inEdFilter.onPressed(mouseX);
      } else if (ouEdFilter.hover_area.contains(mouseX, mouseY)) {
        ouEdFilter.onPressed(mouseX);
      } else if (timeline.hover_area.contains(mouseX, mouseY)) {
        timeline.onPressed(mouseX);
      }
    }
  }
  if(!isHist && !isDrag && !isWheel){
    updateCellSelection();
    isDraw = true;
  }
  loop();
}


void mouseWheel(MouseEvent event) {
  map.mouseWheel(event.getAmount());
  isDraw = true;
  loop();
}


void keyPressed(){
  if(key == '1' || key == '!'){
    isGrid = !isGrid;
    isDraw = true;
    loop();
  }
  else if(key == '2' || key == '@'){
    isTrips = !isTrips;
    isDraw = true;
    loop();
  }
  else if(key == '3' || key == '#'){
    isPoints = !isPoints;
    isDraw = true;
    loop();
  }
  else if(key == '0' || key == ')'){
    isInterface = !isInterface;
    loop();
  }
  else if(key == ' '){
    isArc = !isArc;
    isDraw = true;
    loop();
  }
  else if(key == '4' || key == '$'){
    isEqual = !isEqual;
    initGrid(); // **** Recreate Grid
    println("Num Clusters: " + clusters.size());
    println("Num Edges: " + edges.size());
  }
  else if(key == '5' || key == '%'){
//    isCilento = !isCilento;
//    isDays = !isDays;
    isFilterNodes = !isFilterNodes;
    isDraw = true;
    loop();
  }
  else if(key == '6' || key == '^'){
    isEdDir = !isEdDir;
    if(!isEdDir){
      selection = new HashSet();
      incomingEdges = new HashSet();
      outgoingEdges = new HashSet();    
    }
    isDraw = true;
    println("Show Edge Dir: " + isEdDir);
    loop();
  }
  else if(key == '7' || key == '&'){
    isInEd = !isInEd;
    if(isInEd) outgoingEdges = new HashSet();
    isDraw = true;
    loop();
  }
  else if(key == '8' || key == '*'){
    isOutEd = !isOutEd;
    if(isOutEd) incomingEdges = new HashSet();
    isDraw = true;
    loop();
  }  
  else if(key == 'z' || key == 'Z'){
    map.setCentre(new Location(mapCen3X, mapCen3Y), zoom3);
    map.setZoomSC(zoomSC3);
    isDraw = true;
    loop();
//    if(curSubdivision < maxSubdivision) curSubdivision++;
//    else curSubdivision = minSubdivision;
//    println("Subdivision Level: " + curSubdivision);
  }
  else if(key == 'x' || key == 'X'){    
//    initGrid();  // **** Recreate Grid
//    println("Num Clusters: " + clusters.size());
//    println("Num Edges: " + edges.size());
    println(map.getCenter() + " , " + map.getZoom() + " , " + map.getZoomSC());
  }
  else if(key == 'm' || key == 'M'){
    saveFrame("######.png");
  }
  
}

void updateTripFilterRange(){
  if(tripFilter != null){
    tripFilter.resetFlags();
    List<String> active = tripFilter.getActiveKeys();
    int[] keys = new int[active.size()];
    for (int i=0; i<active.size (); i++) {
      String key = (String) active.get(i);
      int val = Integer.parseInt(key);
      keys[i] = val;
    }
    Arrays.sort(keys);
    minWtTrip = keys[0];
    maxWtTrip = keys[keys.length - 1];
    println("Trip Filter Range: " + minWtTrip + " - " + maxWtTrip);
  }
}

void updatePpleFilterRange(){
  if(ppleFilter != null){
    ppleFilter.resetFlags();
    List<String> active = ppleFilter.getActiveKeys();
    int[] keys = new int[active.size()];
    for (int i=0; i<active.size (); i++) {
      String key = (String) active.get(i);
      int val = Integer.parseInt(key);
      keys[i] = val;
    }
    Arrays.sort(keys);
    minWtPple = keys[0];
    maxWtPple = keys[keys.length - 1];
    println("People Filter Range: " + minWtPple + " - " + maxWtPple);
  }
}

void updateDistFilterRange(){
  if(distFilter != null){
    distFilter.resetFlags();
    List<String> active = distFilter.getActiveKeys();
    int[] keys = new int[active.size()];
    for (int i=0; i<active.size (); i++) {
      String key = (String) active.get(i);
      int val = Integer.parseInt(key);
      keys[i] = val;
    }
    Arrays.sort(keys);
    minWtDist = keys[0];
    maxWtDist = keys[keys.length - 1];
    println("Dist Filter Range: " + minWtDist + " - " + maxWtDist);
  }
}

void updateInEdFilterRange(){
  if(inEdFilter != null){
    inEdFilter.resetFlags();
    List<String> active = inEdFilter.getActiveKeys();
    int[] keys = new int[active.size()];
    for (int i=0; i<active.size (); i++) {
      String key = (String) active.get(i);
      int val = Integer.parseInt(key);
      keys[i] = val;
    }
    Arrays.sort(keys);
    minWtInEd = keys[0];
    maxWtInEd = keys[keys.length - 1];
    println("Incoming Edges Filter Range: " + minWtInEd + " - " + maxWtInEd);
  }
}


void updateOuEdFilterRange(){
  if(ouEdFilter != null){
    ouEdFilter.resetFlags();
    List<String> active = ouEdFilter.getActiveKeys();
    int[] keys = new int[active.size()];
    for (int i=0; i<active.size (); i++) {
      String key = (String) active.get(i);
      int val = Integer.parseInt(key);
      keys[i] = val;
    }
    Arrays.sort(keys);
    minWtOuEd = keys[0];
    maxWtOuEd = keys[keys.length - 1];
    println("Outgoing Edges Filter Range: " + minWtOuEd + " - " + maxWtOuEd);
  }
}

void updateTimelineRange(){
  if(timeline != null){
    timeline.resetFlags();
    Date[] active = timeline.getActiveRange();
    frDate = active[0];
    Calendar cal = Calendar.getInstance();
    cal.setTime(active[1]);
    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);   
    toDate = cal.getTime();
    println("Time Range: " + frDate + " >> " + toDate);
  }
}

void updateCellSelection(){
  
  for (Location locCN : clusters.keySet ()) {
    PVector[] cellPos = (PVector[]) gridPos.get(locCN);
    if (mouseX > cellPos[0].x && mouseX < cellPos[1].x &&
        mouseY < cellPos[2].y && mouseY > cellPos[1].y) {
          
      // Draw Selection
      noFill();
      stroke(0);
      strokeWeight(2);
      beginShape();
      vertex(cellPos[0].x, cellPos[0].y);
      vertex(cellPos[1].x, cellPos[1].y);
      vertex(cellPos[2].x, cellPos[2].y);
      vertex(cellPos[3].x, cellPos[3].y);
      endShape(CLOSE);
      strokeWeight(1);
      noStroke();
      
      // Add Key to Selection
      if(!selection.contains(locCN)){
          
          // Add the cell to selection
          selection.add(locCN);
          
          // ****** Get Keywords
          // Get the entries in the cluster
          Map<Object, Set<Entry>> selectedEntries = (HashMap) clusters.get(locCN);
          println("Num People: " + selectedEntries.size());
            
          // Aggregate keywords
          Map<String, Integer> mentions = new HashMap();
          Map<String, Set<Object>> people = new HashMap();
          for(Object id : selectedEntries.keySet()){
            Set<Entry> entries = (HashSet) selectedEntries.get(id);
            for(Entry e : entries){
              if(e.keywords != null){
                for(String word : e.keywords){
                  addToMap(mentions, word);
                  addToMap(people, word, id);
                }
              }
            }
          }
          mentions = sortByValue(mentions, true);
          int cn = 0;
          for(String word : mentions.keySet()){
            if(cn > 99) break;
            int val = (Integer) mentions.get(word);
            Set<Object> ids = (HashSet) people.get(word);
            println(word + ", " + val + ", " + ids.size());
            cn++;
          }
          int v = mentions.size() - 100;
          if(v > 0) println("And " + v + " more...");
          println();
          // ****** Get Keywords
          
//          // Print Coordinates
//          Location l1 = map.pointLocation(cellPos[0]);
//          Location l2 = map.pointLocation(cellPos[1]);
//          Location l3 = map.pointLocation(cellPos[2]);
//          Location l4 = map.pointLocation(cellPos[3]);
//          println("Query Vertices ******");
//          println("[" + l1.lat + "," + l1.lon + "],[" + l2.lat + "," + l2.lon + "],[" + l3.lat + "," + l3.lon + "],[" + l4.lat + "," + l4.lon + "]");
//          println("Query Vertices ******");
//          println();
          
        } else {
          selection.remove(locCN);
        }
        
      break;
      
    }
  }
  println("Num Selected Cells: " + selection.size());
  
  // Get the right Edges
  incomingEdges = new HashSet();
  outgoingEdges = new HashSet();
  if(isEdDir){
    for(Location locCN : selection){
      if(isInEd){
        if(incoming.containsKey(locCN)){
          Set<Edge> subIncomingEdges = (HashSet) incoming.get(locCN);
          if(subIncomingEdges != null) incomingEdges.addAll(subIncomingEdges);
        }
      }
      if(isOutEd){
        if(outgoing.containsKey(locCN)){
          Set<Edge> subOutgoingEdges = (HashSet) outgoing.get(locCN);
          if(subOutgoingEdges != null) outgoingEdges.addAll(subOutgoingEdges);
        }
      }
    }
  }
  println("Num Incoming Edges: " + incomingEdges.size());
  println("Num Outgoing Edges: " + outgoingEdges.size());
  
}



//void updateCellSelection(){
//  for (Location locCN : clusters.keySet ()) {
//      PVector[] cellPos = (PVector[]) gridPos.get(locCN);
//      if (mouseX > cellPos[0].x && mouseX < cellPos[1].x &&
//          mouseY < cellPos[2].y && mouseY > cellPos[1].y) {
//            
//        noFill();
//        stroke(0);
//        strokeWeight(2);
//        beginShape();
//        vertex(cellPos[0].x, cellPos[0].y);
//        vertex(cellPos[1].x, cellPos[1].y);
//        vertex(cellPos[2].x, cellPos[2].y);
//        vertex(cellPos[3].x, cellPos[3].y);
//        endShape(CLOSE);
//        strokeWeight(1);
//        noStroke();
//
//        incomingEdges = (HashSet) incoming.get(locCN);
//        outgoingEdges = (HashSet) outgoing.get(locCN);
//        if(incomingEdges == null) incomingEdges = new HashSet();
//        if(outgoingEdges == null) outgoingEdges = new HashSet();
//
//        println("Num Incoming Edges: " + incomingEdges.size());
//        println("Num Outgoing Edges: " + outgoingEdges.size());
//        
//        // Get the entries in the cluster
//        Map<Object, Set<Entry>> selectedEntries = (HashMap) clusters.get(locCN);
//        println("Num People: " + selectedEntries.size());
//        
//        // Aggregate keywords
//        Map<String, Integer> mentions = new HashMap();
//        Map<String, Set<Object>> people = new HashMap();
//        for(Object id : selectedEntries.keySet()){
//          Set<Entry> entries = (HashSet) selectedEntries.get(id);
//          for(Entry e : entries){
//            if(e.keywords != null){
//              for(String word : e.keywords){
//                addToMap(mentions, word);
//                addToMap(people, word, id);
//              }
//            }
//          }
//        }
//        mentions = sortByValue(mentions, true);
//        int cn = 0;
//        for(String word : mentions.keySet()){
//          if(cn > 9) break;
//          int val = (Integer) mentions.get(word);
//          Set<Object> ids = (HashSet) people.get(word);
//          println(word + ", " + val + ", " + ids.size());
//          cn++;
//        }
//        int v = mentions.size() - 10;
//        if(v > 0) println("And " + v + " more...");
//        println();
//        
//        break;
//        
//      }
//  }
//}



void updateTimeLine(){
  
  // **** Setup Timeline
  Calendar cal = Calendar.getInstance();
  toDate = cal.getTime();
  int toDayOfYear = cal.get(Calendar.DAY_OF_YEAR);

  String inDate = "29/05/2014";
  inFormat  = new SimpleDateFormat("dd/M/yyyy");
  outFormat = new SimpleDateFormat("dd/M");

  try {
    Date d = inFormat.parse(inDate);
    cal.setTime(d);
  } catch (Exception e) {
    println(e);
    println("Timeline Error");
    exit();
  }

  frDate = cal.getTime();
  cal.set(Calendar.HOUR, 0);
  cal.set(Calendar.MINUTE, 0);
  cal.set(Calendar.SECOND, 0);
  cal.set(Calendar.MILLISECOND, 0);
  int frDayOfYear = cal.get(Calendar.DAY_OF_YEAR);

  List<String> timelineLabels = new ArrayList();
  List<String> timelineKeys = new ArrayList();
  List<Integer> timelineIn = new ArrayList();
  List<Integer> timelineOut = new ArrayList();
  List<Date> timelineDates = new ArrayList();
  
  for (int i=frDayOfYear; i<toDayOfYear; i++) {

    cal.set(Calendar.DAY_OF_YEAR, i);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);

    timelineKeys.add(i + "");
    timelineDates.add(cal.getTime());
    
    try {
      timelineLabels.add(outFormat.format(cal.getTime()));
    } catch (Exception e) {
      println("Failed to parse: " + i);
      exit();
    }
    
    Date fr = cal.getTime();

    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);

    Date to = cal.getTime();

    int nEdges = 0;
    int nSelected = 0;
    int nOutgoing = 0;
    int nIncoming = 0;
    for (Object id : edges.keySet ()) {
      Set<Edge> indEdges = (HashSet) edges.get(id);
      Iterator it = indEdges.iterator();
      boolean isSelect = false;
      while (it.hasNext ()) {
        Edge e = (Edge) it.next();
        for (Entry[] entries : e.getEntries ()) {
          Date d1 = entries[0].date;
          Date d2 = entries[1].date;
          if(d1.after(fr) && d1.before(to) 
          ||(d2.after(fr) && d2.before(to))){
            if(entries[0].isFlag){
              nOutgoing++;
            }else if(entries[1].isFlag){
              nIncoming++;
            }
            break;
          }
        }
      }
    }
    timelineIn.add(nIncoming);
    timelineOut.add(nOutgoing);
  }
  
  timeline.incoming = timelineIn;
  timeline.outgoing = timelineOut;
  timeline.setup(30, height-110, 1230, 80, false);
  
}



void getHomeLoc(){

  Map<Object,Map<Location,Integer>> homeLocs = new HashMap();
  
  for(Location loc : clusters.keySet()){
    Map<Object, Set<Entry>> clusterInfo = (HashMap) clusters.get(loc);
    for(Object o : clusterInfo.keySet()){
      Set<Entry> cEntries = (HashSet) clusterInfo.get(o);
      if(homeLocs.containsKey(o)){
        Map<Location,Integer> indClusters = (HashMap) homeLocs.get(o);
        indClusters.put(loc, cEntries.size());
      } else {
        Map<Location,Integer> indClusters = new HashMap();
        indClusters.put(loc, cEntries.size());
        homeLocs.put(o, indClusters);
      }
    }
  }
  
  PrintWriter output = createWriter("home-loc-output.csv");
  for(Object o : homeLocs.keySet()){
    Map<Location,Integer> clusterSizes = (HashMap) homeLocs.get(o);
    clusterSizes = sortByValue(clusterSizes, true);
    for(Location l : clusterSizes.keySet()){
      int count = (Integer) clusterSizes.get(l);
      output.println("\"" + o + "\",\"" +  l.lat + "\",\"" + l.lon + "\",\"" + count + "\"");
      break;
    }
  }
  output.flush();
  output.close();

}
