List<Entry> getEntries(DBCursor inDBCursor, DraggableMap inMap){

  List<Entry> output = new ArrayList();

  while(inDBCursor.hasNext()){
    DBObject curObj = inDBCursor.next();
    
    // Individual Id
    Object id = (Object) curObj.get("_id");
    
    // Get Date
    Date date = (Date) curObj.get("created_at");

    // Get Location
    DBObject comObj = (DBObject) curObj.get("loc");
    Object oLat = (Object) comObj.get("lat");
    Object oLon = (Object) comObj.get("lon");
    float lat = 0;
    float lon = 0;
    
    if(oLat instanceof Integer){
      lat = (Integer) comObj.get("lat");
    } else {
      double d = (Double) comObj.get("lat");
      lat = (float) d;
    }
    if(oLon instanceof Integer){
      lon = (Integer) comObj.get("lon");
    } else {
      double d = (Double) comObj.get("lon");
      lon = (float) d;
    }
    Location loc = new Location(lat,lon);
    
    // Get Screen Position
    PVector scr = inMap.locationPoint(loc);
    
    // Get Keywords
    String[] keywords = null;
    if(curObj.containsField("keywords")){
      BasicDBList words = (BasicDBList) curObj.get("keywords");
      keywords = new String[words.size()];
      for(int i=0; i<words.size(); i++){
        keywords[i] = (String) words.get(i);
      }
    }
    
    // Is the tweet in Cilento
    boolean isCliento = curObj.containsField("cliento");
    
    // Create the Entry to be Added
    Entry entry = new Entry(id, date, loc, keywords, isCliento);
    entry.setPosition(scr);

    output.add(entry);
  }
  
  return output; 

}
