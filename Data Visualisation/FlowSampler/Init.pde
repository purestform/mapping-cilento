void initGrid() {
  println("[Init] - Generate Grid");

  Grid eq = null;

  gridCen = map.pointLocation(new PVector(606, (height/2)));
  centre = map.locationPoint(gridCen);

  // Either equal or quadtree grids
  if (isEqual) {
    eq = new EqualGrid(centre, gridDim, gridDim, curSubdivision);
  } else {
    
    // ******* Use Foursquare for Unequals Grid
    List<PVector> ptPos = new ArrayList();
    for(Location loc : foursquare){
      PVector pos = map.locationPoint(loc);
      ptPos.add(pos);
    }
    // ******* Use Foursquare for Unequals Grid
    
    // ******* Use Tweets for Unequals Grid
    //for (Object id : individuals.keySet ()) {
    //  Individual ind = (Individual) individuals.get(id);
    //  for (Entry e : ind.entries) {
    //    e.position = map.locationPoint(e.location);
    //    ptPos.add(e.position);
    //  }
    //}
    // ******* Use Tweets for Unequals Grid
    
    eq = new QuadTree(centre, gridDim, gridDim, 25);
    eq.subdivide(ptPos, maxSubdivision);
    
  }

  entriesInCells = getEntriesInCells(individuals, eq); 
  
  // Graph
  println("Building Graph");
  clusters = getClusters(individuals);
  edges    = getEdgesByDay(clusters);
  //edges    = getEdgesByInterval(clusters, 48);
  
  daysActiveCells = getDaysActiveCells(clusters); 
  List<Integer> dayCount = new ArrayList(daysActiveCells.values());
  minActiveDays = Collections.min(dayCount);
  maxActiveDays = Collections.max(dayCount);
  println("Cell Active Day Range: " + minActiveDays + " - " + maxActiveDays);
  
  peopleActiveCells = getPeopleActiveCells(clusters);
  List<Integer> ppleCount = new ArrayList(peopleActiveCells.values());
  minPeopleCells = Collections.min(ppleCount);
  maxPeopleCells = Collections.max(ppleCount);
  println("Cell People Range: " + minPeopleCells + " - " + maxPeopleCells);
  
  //getHomeLoc();
  
  println("Indexing Graph");
  incoming      = new HashMap(); // All Incoming
  outgoing      = new HashMap(); // All Outgoing
  incomingEdges = new HashSet(); // Selected Incoming
  outgoingEdges = new HashSet(); // Selected Outgoing
  selection     = new HashSet(); // Selected Cells
  
  for(Object id : edges.keySet()){
    Set<Edge> edgeList = (HashSet) edges.get(id);
    for(Edge e : edgeList){
      addToMap(incoming, e.to, e);
      addToMap(outgoing, e.from, e);
    }
  }
  
  
  // Aggregate Weights
  println("Building Interface");
  Set<Integer> ppleWts = new HashSet();
  Set<Integer> tripWts = new HashSet();
  Set<Integer> distWts = new HashSet();
  for (Object id : edges.keySet ()) {
    Set<Edge> indEdges = (HashSet) edges.get(id);
    for (Edge e : indEdges) {
      
//      if(e.getNumIndividuals() == 1047){
//        println(e.getIndividuals());
//      }
      
      tripWts.add(e.getNumEntries());
      ppleWts.add(e.getNumIndividuals());
      distWts.add(e.getDistance());
    }
  }
  Set<Integer> inEdWts = new HashSet(); 
  for (Location l : incoming.keySet ()) {
    Set<Edge> inEdges = (HashSet) incoming.get(l);
    inEdWts.add(inEdges.size());
  }
  Set<Integer> ouEdWts = new HashSet(); 
  for (Location l : outgoing.keySet ()) {
    Set<Edge> ouEdges = (HashSet) outgoing.get(l);
    ouEdWts.add(ouEdges.size());
  }

  // Get minimum & maximum edge weights
  // Arrange weights in ascending order
  ppleRange = getExtrema(ppleWts);
  tripRange = getExtrema(tripWts);
  distRange = getExtrema(distWts);
  inEdRange = getExtrema(inEdWts);
  ouEdRange = getExtrema(ouEdWts);
  
  println("Pple Range Init: " + ppleRange[0] + "," + ppleRange[1]);
  println("Trip Range Init: " + tripRange[0] + "," + tripRange[1]);
  println("Dist Range Init: " + distRange[0] + "," + distRange[1]);
  println("InEd Range Init: " + inEdRange[0] + "," + inEdRange[1]);
  println("OuEd Range Init: " + ouEdRange[0] + "," + ouEdRange[1]);
  
  // Create filter interface
  // Use Log scale for histogram bins
  ppleScale = new ScaleMap(log(ppleRange[0]), log(ppleRange[ppleRange.length-1]), 1, nBins);
  tripScale = new ScaleMap(log(tripRange[0]), log(tripRange[tripRange.length-1]), 1, nBins);
  inEdScale = new ScaleMap(log(inEdRange[0]), log(inEdRange[inEdRange.length-1]), 1, nBins);
  ouEdScale = new ScaleMap(log(ouEdRange[0]), log(ouEdRange[ouEdRange.length-1]), 1, nBins);

  // Get histogram labels & values
  Map<Integer, Integer> ppleHistMap = new LinkedHashMap();
  Map<Integer, Integer> tripHistMap = new LinkedHashMap();
  Map<Integer, Integer> distHistMap = new LinkedHashMap();
  Map<Integer, Integer> inEdHistMap = new LinkedHashMap();
  Map<Integer, Integer> ouEdHistMap = new LinkedHashMap();
  Map<Integer,Set<Integer>> ppleHistLabelAgg = new HashMap();
  Map<Integer,Set<Integer>> tripHistLabelAgg = new HashMap();
  Map<Integer,Set<Integer>> distHistLabelAgg = new HashMap();
  Map<Integer,Set<Integer>> inEdHistLabelAgg = new HashMap();
  Map<Integer,Set<Integer>> ouEdHistLabelAgg = new HashMap();
  
  for (Object id : edges.keySet ()) {
    Set<Edge> indEdges = (HashSet) edges.get(id);
    Iterator it = indEdges.iterator();
    while (it.hasNext ()) {
      Edge e = (Edge) it.next();
      int nPple    = e.getNumIndividuals();
      int logPple  = (int) ppleScale.getMappedValueFor(log(nPple));
      addToMap(ppleHistMap, logPple);
      addToMap(ppleHistLabelAgg, logPple, nPple);
      int nTrips   = e.getNumEntries();
      int logTrips = (int) tripScale.getMappedValueFor(log(nTrips));
      addToMap(tripHistMap, logTrips);
      addToMap(tripHistLabelAgg, logTrips, nTrips);
      int nDists   = e.getDistance();
      int linDists = (int) map(nDists, distRange[0], distRange[1], 0, nBins);
      addToMap(distHistMap, linDists);
      addToMap(distHistLabelAgg, linDists, nDists);
    }
  }
  for (Location l : incoming.keySet ()) {
    Set<Edge> inEdges = (HashSet) incoming.get(l);
    int log = (int) inEdScale.getMappedValueFor(log(inEdges.size()));
    addToMap(inEdHistMap, log);
    addToMap(inEdHistLabelAgg, log, inEdges.size());
  }
  for (Location l : outgoing.keySet ()) {
    Set<Edge> ouEdges = (HashSet) outgoing.get(l);
    int log = (int) ouEdScale.getMappedValueFor(log(ouEdges.size()));
    addToMap(ouEdHistMap, log);
    addToMap(ouEdHistLabelAgg, log, ouEdges.size());
  }
 
  List<String> tripKeys = new ArrayList();
  List<String> ppleKeys = new ArrayList();
  List<String> distKeys = new ArrayList();
  List<String> inEdKeys = new ArrayList();
  List<String> ouEdKeys = new ArrayList();
  for (Integer i : tripHistMap.keySet ()) tripKeys.add(i + "");
  for (Integer i : ppleHistMap.keySet ()) ppleKeys.add(i + "");
  for (Integer i : distHistMap.keySet ()) distKeys.add(i + "");
  for (Integer i : inEdHistMap.keySet ()) inEdKeys.add(i + "");
  for (Integer i : ouEdHistMap.keySet ()) ouEdKeys.add(i + "");
    
  List<Integer> sortTripKeys = new ArrayList(tripHistMap.keySet());
  List<Integer> sortPpleKeys = new ArrayList(ppleHistMap.keySet());
  List<Integer> sortDistKeys = new ArrayList(distHistMap.keySet());
  List<Integer> sortInEdKeys = new ArrayList(inEdHistMap.keySet());
  List<Integer> sortOuEdKeys = new ArrayList(ouEdHistMap.keySet());
  Collections.sort(sortTripKeys);
  Collections.sort(sortPpleKeys);
  Collections.sort(sortDistKeys);
  Collections.sort(sortInEdKeys);
  Collections.sort(sortOuEdKeys);
  
  List<Integer> ppleVals = new ArrayList();
  for (Integer i : ppleHistMap.keySet ()) {
    int val = (Integer) ppleHistMap.get(i);
    ppleVals.add(val);
  }
  
  List<String> ppleLabels = new ArrayList();
  for (Integer i : sortPpleKeys) {
    Set<Integer> val = (HashSet) ppleHistLabelAgg.get(i);
    int[] valRange = getExtrema(val);
    String label = "";
    if(valRange[0] == valRange[1]) label = "" + valRange[0];
    else label = valRange[0] + " - " + valRange[1];
    ppleLabels.add(label);
  }
  println("Pple Size: " + ppleKeys.size());
  println("Pple Size: " + ppleVals.size());

  ppleFilter = new Histogram(ppleKeys, ppleVals, true);
  ppleFilter.name = new String("Number of People").toUpperCase();
  ppleFilter.labels = ppleLabels;
  ppleFilter.setup(width-360, 30, 320, 50);
  updatePpleFilterRange();


  List<String> tripLabels = new ArrayList();
  List<Integer> tripVals = new ArrayList();
  for (Integer i : tripHistMap.keySet ()) {
    int val = (Integer) tripHistMap.get(i);
    tripVals.add(val);
  }
  
  for (Integer i : sortTripKeys) {
    Set<Integer> val = (HashSet) tripHistLabelAgg.get(i);
    int[] valRange = getExtrema(val);
    String label = "";
    if(valRange[0] == valRange[1]) label = "" + valRange[0];
    else label = valRange[0] + " - " + valRange[1];
    tripLabels.add(label);
  }
  println("Trip Key Size: " + tripKeys.size());
  println("Trip Val Size: " + tripVals.size());

  tripFilter = new Histogram(tripKeys, tripVals, true);
  tripFilter.name = new String("Number of Trips").toUpperCase();
  tripFilter.labels = tripLabels;
  tripFilter.setup(width-360, 130, 320, 50);
  updateTripFilterRange();

  List<String> distLabels = new ArrayList();
  List<Integer> distVals = new ArrayList();
  for (Integer i : distHistMap.keySet ()) {
    int val = (Integer) distHistMap.get(i);
    distVals.add(val);
  }
  
  for (Integer i : sortDistKeys) {
    Set<Integer> val = (HashSet) distHistLabelAgg.get(i);
    int[] valRange = getExtrema(val);
    String label = "";
    if(valRange[0] == valRange[1]) label = "" + valRange[0];
    else if(valRange[1] < valRange[0]) label = valRange[1] + " - " + valRange[0];
    else label = valRange[0] + " - " + valRange[1];
    distLabels.add(label);
  }
  println("Dist Key Size: " + distKeys.size());
  println("Dist Val Size: " + distVals.size());

  distFilter = new Histogram(distKeys, distVals, true);
  distFilter.name = new String("Range of Distances").toUpperCase();
  distFilter.labels = distLabels;
  distFilter.setup(width-360, 230, 320, 50);
  updateDistFilterRange();
  
  List<Integer> inEdVals = new ArrayList();
  for (Integer i : inEdHistMap.keySet ()) {
    int val = (Integer) inEdHistMap.get(i);
    inEdVals.add(val);
  }  
  List<String> inEdLabels = new ArrayList();
  for (Integer i : sortInEdKeys) {
    Set<Integer> val = (HashSet) inEdHistLabelAgg.get(i);
    int[] valRange = getExtrema(val);
    String label = "";
    if(valRange[0] == valRange[1]) label = "" + valRange[0];
    else label = valRange[0] + " - " + valRange[1];
    inEdLabels.add(label);
  }
  println("InEd Key Size: " + inEdKeys.size());
  println("InEd Val Size: " + inEdVals.size());
  
  inEdFilter = new Histogram(inEdKeys, inEdVals, true);
  inEdFilter.name = new String("Number of Incoming Edges").toUpperCase();
  inEdFilter.labels = inEdLabels;
  inEdFilter.setup(width-360, 330, 320, 50);
  updateInEdFilterRange();   
  
  List<Integer> ouEdVals = new ArrayList();
  for (Integer i : ouEdHistMap.keySet ()) {
    int val = (Integer) ouEdHistMap.get(i);
    ouEdVals.add(val);
  }  
  List<String> ouEdLabels = new ArrayList();
  for (Integer i : sortOuEdKeys) {
    Set<Integer> val = (HashSet) ouEdHistLabelAgg.get(i);
    int[] valRange = getExtrema(val);
    String label = "";
    if(valRange[0] == valRange[1]) label = "" + valRange[0];
    else label = valRange[0] + " - " + valRange[1];
    ouEdLabels.add(label);
  }
  println("OuEd Key Size: " + ouEdKeys.size());
  println("OuEd Val Size: " + ouEdVals.size());
  
  ouEdFilter = new Histogram(ouEdKeys, ouEdVals, true);
  ouEdFilter.name = new String("Number of Outgoing Edges").toUpperCase();
  ouEdFilter.labels = ouEdLabels;
  ouEdFilter.setup(width-360, 430, 320, 50);
  updateOuEdFilterRange();   

  // **** Setup Timeline
  inFormat  = new SimpleDateFormat("dd/M/yyyy");
  outFormat = new SimpleDateFormat("dd/M");  
  Calendar cal = Calendar.getInstance();
  String inToDate = "31/12/2014";
  try {
    Date d = inFormat.parse(inToDate);
    cal.setTime(d);
  } catch (Exception e) {
    println(e);
    println("Timeline Error");
    exit();
  }  
  toDate = cal.getTime();
  int toDayOfYear = cal.get(Calendar.DAY_OF_YEAR);

  String inFrDate = "29/05/2014";
  try {
    Date d = inFormat.parse(inFrDate);
    cal.setTime(d);
  } catch (Exception e) {
    println(e);
    println("Timeline Error");
    exit();
  }

  frDate = cal.getTime();
  cal.set(Calendar.HOUR, 0);
  cal.set(Calendar.MINUTE, 0);
  cal.set(Calendar.SECOND, 0);
  cal.set(Calendar.MILLISECOND, 0);
  int frDayOfYear = cal.get(Calendar.DAY_OF_YEAR);

  List<String> timelineLabels = new ArrayList();
  List<String> timelineKeys = new ArrayList();
  List<Integer> timelineVals = new ArrayList();
  List<Integer> timelineSelc = new ArrayList();
  List<Date> timelineDates = new ArrayList();
  
  for (int i=frDayOfYear; i<toDayOfYear; i++) {

    cal.set(Calendar.DAY_OF_YEAR, i);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);

    timelineKeys.add(i + "");
    timelineDates.add(cal.getTime());
    
    try {
      timelineLabels.add(outFormat.format(cal.getTime()));
    } catch (Exception e) {
      println("Failed to parse: " + i);
      exit();
    }
    
    Date fr = cal.getTime();

    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);

    Date to = cal.getTime();

    int nEdges = 0;
    int nSelected = 0;
    for (Object id : edges.keySet ()) {
      Set<Edge> indEdges = (HashSet) edges.get(id);
      Iterator it = indEdges.iterator();
      boolean isSelect = false;
      while (it.hasNext ()) {
        Edge e = (Edge) it.next();
        for (Entry[] entries : e.getEntries ()) {
          Date d1 = entries[0].date;
          Date d2 = entries[1].date;
          if(d1.after(fr) && d1.before(to) 
          ||(d2.after(fr) && d2.before(to))){
            nEdges++;
            if(entries[0].isFlag || entries[1].isFlag){
              nSelected++;
            }
            break;
          }
        }        
      }
    }
    timelineVals.add(nEdges);
    timelineSelc.add(nSelected);
  }
  
  
  
  /* trace timeline values */
  
  for(int i=0; i<timelineLabels.size(); i++){
    String label = (String) timelineLabels.get(i);
    int value = (Integer) timelineVals.get(i);
    println("\"" + label + "\"" + "," + "\"" + value + "\"");
  }
  
  /* trace timeline values */



  timeline = new TimeLine(timelineKeys, timelineDates, timelineVals, timelineSelc, true);
  timeline.labels = timelineLabels;
  timeline.setup(30, height-110, 1230, 80, false);
  updateTimelineRange();
  
  // **** Setup Timeline
  Cell cellStart = (Cell) eq.grid.get(0);
  List<Integer> cellValues = new ArrayList();
  for (int i=1; i<eq.grid.size (); i++) {
    Cell c = (Cell) eq.grid.get(i);
    cellValues.add(c.count);
  }
  int maxPts = Collections.max(cellValues);
  int minPts = Collections.min(cellValues); 
  println("Timeline Min: " + minPts);
  println("Timeline Max: " + maxPts);

  gridLoc = new HashMap();
  gridPos = new HashMap();
  gridWid = new HashMap();
  for (int i=0; i<eq.grid.size (); i++) {
    Cell c = (Cell) eq.grid.get(i);
    PVector topLeft  = new PVector(c.centre.x - (c.width/2), c.centre.y - (c.height/2));
    PVector btmRight = new PVector(c.centre.x + (c.width/2), c.centre.y + (c.height/2));
    Location locCN = map.pointLocation(c.centre);
    Location locTL = map.pointLocation(topLeft);
    Location locBR = map.pointLocation(btmRight);
    Location[] geometry = {
      locTL, locBR
    };
    gridLoc.put(locCN, geometry);
    gridWid.put(locCN, c.width);
  }
  
  updateGridPos();
  uniqueID = getUniqueID(gridPos);

  eq.grid.clear();
  eq = null;

  isDraw = true;

}



Map<Location, Integer> getDaysActiveCells (Map<Location, Map<Object, Set<Entry>>> inClusters){
  Calendar c = Calendar.getInstance();
  Map<Location, Integer> output = new HashMap();
  for(Location l : inClusters.keySet()){
    Set<Integer> daysOfYear = new HashSet();
    Map<Object, Set<Entry>> clusterEntries = (HashMap) inClusters.get(l);
    for(Object id : clusterEntries.keySet()){
      Set<Entry> personEntries = (HashSet) clusterEntries.get(id);
      Iterator it = personEntries.iterator();
      while(it.hasNext()){
        Entry e = (Entry) it.next();
        c.setTime(e.getDate());
        int dayOfYear = c.get(Calendar.DAY_OF_YEAR);
        daysOfYear.add(dayOfYear);
      }
    }
    if(daysOfYear.size() > 0){
      output.put(l, daysOfYear.size());
    }
  }
  return output;
}



Map<Location, Integer> getPeopleActiveCells (Map<Location, Map<Object, Set<Entry>>> inClusters){
  Map<Location, Integer> output = new HashMap();
  for(Location l : inClusters.keySet()){
    Map<Object, Set<Entry>> clusterEntries = (HashMap) inClusters.get(l);
    if(clusterEntries.size() > 0){
      output.put(l, clusterEntries.size());
    }
  }
  return output;
}


Map<Location, Integer> getUniqueID(Map<Location, PVector[]> inGridPos){
  
  Map<Location, Integer> output = new HashMap();
  for (Location locCN : inGridPos.keySet ()) {
    output.put(locCN, output.size());
  }
  return output;
  
}
