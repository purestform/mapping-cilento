Map<Location,Map<Object,Set<Entry>>> getClusters(Map<Object, Individual> inputMap){
  
  Map<Entry, Object> entryToInd = new HashMap();
  
  for(Object id: inputMap.keySet()){
    Individual ind = (Individual) inputMap.get(id);
    List<Entry> entries = ind.getEntries();
    for(Entry e : entries){
      entryToInd.put(e, id);
    }
  }
  
  List<Entry> aggEntries = new ArrayList(entryToInd.keySet());
  Map<Location, Set<Entry>> clusters = getClusters(aggEntries);
  Map<Set<Entry>, Location> clusterRef = reverseMap(clusters);
  Map<Location,Map<Object,Set<Entry>>> output = new HashMap();
  
  
  for(Object id : inputMap.keySet()){
    Individual ind = (Individual) inputMap.get(id);
    List<Entry> entries = ind.getEntries();
    for(Entry e : entries){
      for(Set<Entry> clusterEntries : clusterRef.keySet()){
        if(clusterEntries.contains(e)){
          Location loc = (Location) clusterRef.get(clusterEntries);
          if(output.containsKey(loc)){
            Map<Object,Set<Entry>> indEntryMap = (HashMap) output.get(loc);
            if(indEntryMap.containsKey(id)){
              Set<Entry> indEntries = (HashSet) indEntryMap.get(id);
              indEntries.add(e);
            } else {
              Set<Entry> indEntries = new HashSet();
              indEntries.add(e);
              indEntryMap.put(id, indEntries);
            }
          } else {
            Map<Object,Set<Entry>> indEntryMap = new HashMap();
            Set<Entry> indEntries = new HashSet();
            indEntries.add(e);
            indEntryMap.put(id, indEntries);
            output.put(loc, indEntryMap);
          }
          break;
        }
      }
    }
  }
  
  
  return output;
  
}



Map<Location,Set<Entry>> getClusters(List<Entry> inputList) {
 
  // Output clustering
  Map<Location,Set<Entry>> clusters = new HashMap();
  
  for (Cell c : entriesInCells.keySet()) {
    Location cellCentroid = map.pointLocation(c.centre);
    Set<Entry> cellEntries = new HashSet();
    Map<Object, List<Entry>> individualsInCell = (HashMap) entriesInCells.get(c);
    for(Object id : individualsInCell.keySet()){
      List<Entry> entries = (ArrayList) individualsInCell.get(id);
      cellEntries.addAll(entries);
    }
    clusters.put(cellCentroid, cellEntries);
  }
  
  return clusters;
  
}


Map<Set<Entry>, Location> reverseMap(Map<Location, Set<Entry>> inputMap){
  Map<Set<Entry>, Location> output = new HashMap();
  for(Location loc : inputMap.keySet()){
    Set<Entry> entry = (HashSet) inputMap.get(loc);
    output.put(entry, loc);
  }
  return output;
}
