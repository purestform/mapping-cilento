![flowsampler.png](https://bitbucket.org/repo/oX4kdG/images/3357248575-flowsampler.png)

### Visualisation ###
* For an introduction of the software features, [please read our publication](http://link.springer.com/chapter/10.1007/978-3-319-15168-7_2).
* You can watch a [demo video here](https://vimeo.com/116299265).
* Please [cite this work](http://dblp.uni-trier.de/rec/bibtex/conf/socinfo/ChuaMSM14) if you find it useful :)
* Requires [geotools](https://bitbucket.org/purestform/geotools) library.

### Data Collection, Processing & Visualisation Pipeline ###
A short description of each module is provided in the [wiki](https://bitbucket.org/purestform/mapping-cilento/wiki/Home).
 
![3e8e60ffbb556dbc1bac5b6fd205ada3078690fe.png](https://bitbucket.org/repo/oX4kdG/images/3979954899-3e8e60ffbb556dbc1bac5b6fd205ada3078690fe.png)

### Coding Environment ###
* For details on Twitter python library, see [tweepy](https://github.com/tweepy/tweepy).
* For details on .pde script, see [processing.org](http://www.processing.org).
* For details on MongoDB query syntax, see [mongodb.org](https://docs.mongodb.org/manual/).
* For details on MongoDB python drivers, see [mongodb.org](https://docs.mongodb.org/ecosystem/drivers/python/).