import com.mongodb.*;
import com.mongodb.io.*;
import com.mongodb.util.*;
import com.mongodb.gridfs.*;

import org.bson.*;
import org.bson.io.*;
import org.bson.util.*;
import org.bson.types.*;

import java.util.*;

void setup(){
  
  String ip         = "127.0.0.1";
  String password   = "xxxx";
  String username   = "xxxx";  

  String db         = "xxxx";
  String people     = "xxxx";
  String tweets     = "xxxx";
  
  MongoDB m = new MongoDB(ip, db, username, password);
  m.setCollection(tweets);
  
  DBObject isExists   = new BasicDBObject("$exists",true);
  DBObject inEvent    = new BasicDBObject("event",isExists);
  
  List<Object> ids = m.getDistinct("from_user_id"); 
  m.setCollection(people);

  for(Object id : ids){
    DBObject user = new BasicDBObject("user_id", id);
    m.insert(user);
  }
  
  exit();
  
}