String ip        = "127.0.0.1";
String password  = "xxxx";
String username  = "xxxx";
String db        = "xxxx";
String colPeople = "xxxx";
String colTweets = "xxxx";

void setup(){
  
  MongoDB m = new MongoDB(ip, db, username, password);
  m.setCollection(colPeople);
  
  List<Object> ids = m.getDistinct("user_id");

  for(int i=ids.size()-1; i>-1; i--){
    Object id = (Object) ids.get(i);  
    getNumTweetsForPerson(colPeople, colTweets, id);
  }
  
  exit();
  
}

void getNumTweetsForPerson(String inColPeople, String inColTweets, Object inObj){
  
  DBObject queryTweets = new BasicDBObject("from_user_id", inObj);
  m.setCollection(inColTweets);
  int num = (int) m.getCount(queryTweets);
  
  DBObject queryUser = new BasicDBObject("user_id", inObj);
  m.setCollection(inColPeople);
  m.update(queryUser, "tweets", num);
  
}