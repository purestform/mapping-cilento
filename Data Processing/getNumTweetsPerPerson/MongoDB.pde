import com.mongodb.*;
import com.mongodb.io.*;
import com.mongodb.util.*;
import com.mongodb.gridfs.*;

import org.bson.*;
import org.bson.io.*;
import org.bson.util.*;
import org.bson.types.*;

import java.util.*;


class MongoDB {
  
  Mongo m;
  DB db;
  DBCollection c;
  
  MongoDB(String inputIP, String inputDBName, String inputUser, String inputPassword){
    try {
      this.m = new Mongo( inputIP );
      this.db = this.m.getDB("admin");
      this.db.authenticate(inputUser, inputPassword.toCharArray());
      this.db = this.m.getDB( inputDBName );
      System.out.println("**** Connection Successful");
    } catch (Exception e) {
      System.out.println("**** Connection Failed");
      exit();
    }
  }
  
  void setDB(String inputDBName){
    try {
      this.db = this.m.getDB( inputDBName );
    } catch (Exception e) {
      System.out.println("**** Connection To " + inputDBName + " Failed");
      exit();
    }
  }  
  
  void setCollection(String inputColName){
    try {
      this.c = this.db.getCollection(inputColName);
    }  catch (Exception e) {
      System.out.println("**** Connection Failed");
      System.out.println("**** Exit");
      exit();
    }
  }
  
  DBObject getOne(DBObject inputQuery){
    return this.c.findOne(inputQuery);
  }

  DBCursor getData(){
    return this.c.find();
  }  
  
  DBCursor getData(DBObject inputQuery){
    return this.c.find(inputQuery);
  }
  
  List<Object> getDistinct(String inputField){
    return this.c.distinct(inputField);
  }
  
  List<Object> getDistinct(String inputField, DBObject inputQuery){
    return this.c.distinct(inputField, inputQuery);
  }  
  
  long getCount(){
    return this.c.count();
  }  
  
  long getCount(DBObject inputQuery){
    return this.c.count(inputQuery);
  }
  
  void update(DBObject inputQuery, String inputField, Object inputVal){
    this.c.update(inputQuery, 
      new BasicDBObject("$set", 
        new BasicDBObject(inputField, inputVal)
      )
    ); 
  }
  
  void updateMultiple(DBObject inputQuery, String inputField, Object inputVal){
    this.c.update(
      inputQuery, 
      new BasicDBObject(
        "$set", 
        new BasicDBObject(
          inputField, 
          inputVal)
        ), 
      false, 
      true
    );
  }

  void insert(DBObject inputObject){
    this.c.insert(inputObject);
  }  
  
}


static float getFloat(DBObject obj, String inputKey){
  float floatVal = new Float(obj.get(inputKey).toString());
  return floatVal;
}

static float getFloat(Object obj, String inputKey){
  float floatVal = new Float(((BasicBSONObject) obj).get(inputKey).toString());
  return floatVal;
}

static String[] getStringArray(DBObject obj, String inputKey){
  List<String> buffer = new ArrayList();
  BasicDBList stringDBList = (BasicDBList) obj.get(inputKey);
  for(int i=0; i<stringDBList.size(); i++){
    String s = (String) stringDBList.get(i);
    buffer.add(s);
  }
  String[] output = new String[buffer.size()];
  for(int i=0; i<buffer.size(); i++){
    output[i] = (String) buffer.get(i);
  }
  return output;
}

DBObject getRangeQuery(Object inputLow, Object inputHigh){
  return BasicDBObjectBuilder.start("$gte", inputLow).add("$lte", inputHigh).get();
}
