String ip                     = "127.0.0.1";
String password               = "xxxx";
String username               = "xxxx";

String sourceDB               = "xxxx";
String sourceCol              = "xxxx";
String targetDB               = "xxxx";
String targetColPeople        = "xxxx";
String targetColTweets        = "xxxx";
String targetColTweetSubset   = "xxxx";

void setup(){
  
  MongoDB m = new MongoDB(ip, db, username, password);
  m.setCollection(targetColPeople);
  
  List<Object> ids = m.getDistinct("unique");
  
  m.setCollection(targetColTweetSubset);
  Set<Object> added = new HashSet(m.getDistinct("unique"));
  
  Iterator it = ids.iterator();
  while(it.hasNext()){
    Object id = (Object) it.next();
    if(added.contains(id)){
      it.remove();
    }
  }

  for(int i=ids.size()-1; i>0; i--){
    Object id = (Object) ids.get(i);
    getAllTweetsForPerson(sourceDB, sourceCol, targetDB, targetColTweets, targetColTweetSubset, id);
  }
  
  exit();
  
}


void getAllTweetsForPerson(String inSourceDB, String inSourceCol String inTargetDB, String inTargetTweetCol, String inTargetTweetSubsetCol, Object inUserID){
  
  DBObject query = new BasicDBObject("from_user_id", inUserID);
  
  m.setDB(inTargetDB);
  m.setCollection(inTargetTweetCol); 
  int exist = (int) m.getCount(query);

  m.setDB(inSourceDB);
  m.setCollection(inSourceCol);
  int current = (int) m.getCount(query);
  
  if(exist < current){
    DBCursor cursor = m.getData(query);
    m.setDB(inTargetDB);
    m.setCollection(inTargetTweetCol);   
    while(cursor.hasNext()){
      DBObject curObj = cursor.next();
      m.insert(curObj);
    }
  }
  
  m.setDB(inTargetDB);
  m.setCollection(inTargetTweetSubsetCol); 
  m.insert(new BasicDBObject("unique", inUserID));  
  
}
