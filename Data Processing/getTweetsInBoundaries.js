var inputPolygon = {
	"p1": {"x":12.345, "y":67.890},
	"p2": {"x":12.345, "y":67.890},
	"pn": {"x":12.345, "y":67.890}
}

var db = db.getMongo().getDB( "inputDB" );
db.inputCollection.ensureIndex( { "loc" : "2d" } );
db.inputCollection.find({ 
	"$and" : [
		{ "loc" : { "$within" : { "$polygon" : inputPolygon } } },
		{ "isWithinPolygon" : { "$exists" : false } } ] 
}).forEach(function(doc){
	db.inputCollection.update(
		{ "_id" : doc._id }, 
		{ "$set" : { "isWithinPolygon" : true } }, 
		false, 
		false
	)
});
