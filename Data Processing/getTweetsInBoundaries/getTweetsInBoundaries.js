var inputPolygon = {
	"p1": {"x":40.50413, "y":15.032441},
	"p2": {"x":40.503178, "y":15.030817},
	"pn": ...
}

var db = db.getMongo().getDB( "inputDB" );
db.inputCollection.ensureIndex( { "loc" : "2d" } );
db.tweetCollection.find({ 
	"$and" : [
		{ "loc" : { "$within" : { "$polygon" : inputPolygon } } },
		{ "isWithinPolygon" : { "$exists" : false } } ] 
}).forEach(function(doc){
	db.tweetCollection.update(
		{ "_id" : doc._id }, 
		{ "$set" : { "isWithinPolygon" : true } }, 
		false, 
		false
	)
});