import java.util.*;
import java.util.regex.*;


String ip        = "127.0.0.1";
String password  = "xxxx";
String username  = "xxxx";
String db        = "xxxx";
String col       = "xxxx";


void setup() {
   

  MongoDB m = new MongoDB(ip, db, username, password);
  m.setCollection(col);

  DBObject query  = new BasicDBObject("foursquare", new BasicDBObject("$exists", false));
  DBCursor cursor = m.getData(query, 50000);
  while(cursor.hasNext()) {
    
    boolean isFoursq  = false;

    DBObject dbObj    = (BasicDBObject) cursor.next();
    Long id           = (Long) dbObj.get("_id");
    String text       = (String) dbObj.get("text");
    
    int start = text.indexOf(" (@ ");
    if((start > -1)){
      int stop = text.indexOf(") ");
      if((stop > -1)){
        if(start < stop){
          isFoursq = containsURL(text);
        }
      }
    }
    
    if(isFoursq){
      DBObject queryObj = new BasicDBObject("_id", id);
      m.update(queryObj, "foursquare", true);
    }
    
  }
  
  cursor.close();
  exit();
  
}

boolean containsURL(String input){
  String regex = "\\(?\\b((https?|ftp)(:|;)//.|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
  Pattern p = Pattern.compile(regex);
  Matcher m = p.matcher(input);
  boolean output = false;
  while (m.find ()) {
    output = true;
    break;
  }  
  return output;
}
