import twitter4j.util.*;
import twitter4j.*;
import twitter4j.management.*;
import twitter4j.api.*;
import twitter4j.conf.*;
import twitter4j.json.*;
import twitter4j.auth.*;

import java.io.*;


Twitter twitter;
int count;
int waitTime;
int currTime;
int timeAtRateLimit;
int withNames;
List<Object> ids;

MongoDB m;

String ip        = "127.0.0.1";
String password  = "xxxx";
String username  = "xxxx";
String db        = "xxxx";
String col       = "xxxx";


void setup(){
  
  m = new MongoDB(ip, db, username, password);
  m.setCollection(col);

  DBObject numPple = new BasicDBObject("name", new BasicDBObject("$exists", true));
  withNames = (int) m.getCount(numPple);
  
  twitter = TwitterFactory.getSingleton(); 
  twitter.setOAuthConsumer("xxxx", "xxxx");
  twitter.setOAuthAccessToken(new AccessToken("xxxx", "xxxx"));

  DBObject isExists = new BasicDBObject("$exists", false);
  DBObject ds = new BasicDBObject("description", isExists);
  DBObject nm = new BasicDBObject("name", isExists);
  DBObject ln = new BasicDBObject("language", isExists);
  BasicDBList dbList = new BasicDBList();
  dbList.put(0, ds);
  dbList.put(1, nm);
  dbList.put(2, ln);
  DBObject query = new BasicDBObject("$and", dbList);
  ids = m.getDistinct("user_id", query);
  
}


void draw(){
  
  if(waitTime == 0){
    
    if(count < ids.size()){
      Object o = (Object) ids.get(count);
      
      try {
        
        User user = null;
        if(o instanceof Integer){
          int id = (Integer) o;
          user = twitter.showUser(id);
        } else {
          long id = (Long) o;
          user = twitter.showUser(id);
        }
        
        String description  = user.getDescription();
        String language     = user.getLang();
        String name         = user.getName();
        String timezone     = user.getTimeZone();
        
        if(!description.equals("") && description != null){
          DBObject queryUser = new BasicDBObject("user_id",o);
          m.update(queryUser, "description", description);            
        }
        
        if(!language.equals("") && language != null){
          DBObject queryUser = new BasicDBObject("user_id",o);
          m.update(queryUser, "language", language);            
        }
        
        if(!name.equals("") && name != null){
          DBObject queryUser = new BasicDBObject("user_id",o);
          m.update(queryUser, "name", name);            
        }
        
        if(!timezone.equals("") && timezone != null){
          DBObject queryUser = new BasicDBObject("user_id",o);
          m.update(queryUser, "timezone", timezone);            
        }
        
      } catch (TwitterException e) {

          if(e.exceededRateLimitation()){
            RateLimitStatus status = e.getRateLimitStatus();
            waitTime = status.getSecondsUntilReset();
            timeAtRateLimit = millis();
            DBObject isExists = new BasicDBObject("$exists", true);
            DBObject numPple = new BasicDBObject("name", isExists);
            withNames = (int) m.getCount(numPple);
          }

      } catch (Exception e) {
        println(e);
      }
      
      count++;

    } else {
      exit();
    }
    
  } else {
    
    int elapsed = (millis() - timeAtRateLimit);
    int elapsedInSecs = (elapsed/1000);
    if(elapsedInSecs != currTime){
      currTime = elapsedInSecs;
    }
    
    if(elapsed > (waitTime*1000)){
      timeAtRateLimit = 0;
      waitTime = 0;
    }
    
  }
 
}
