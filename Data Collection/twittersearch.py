import tweepy
from tweepy import Cursor
from tweepy import TweepError
import time
from datetime import datetime
from pymongo import Connection
import sys

print 'Start'

connection = Connection()
username = 'xxxx'
password = 'xxxx'
connection['xxxx'].authenticate(username, password)

connection = Connection()
db = connection.sarleno
twitterDB = db.isGeo

tweetcount = 0
totalTweets = 0
savedTweets = 0
newestDate = datetime.fromtimestamp(0)
oldestDate = datetime.utcnow()

# tweepy auth
auth = tweepy.auth.OAuthHandler('xxxx', 'xxxx')
auth.set_access_token('xxxx', 'xxxx')
api = tweepy.API(auth)

try:
    while True:
        while tweetcount<9999999:
            
            try:

                result = api.search(geocode='40.276431,15.076686,160km', rpp=100, result_type='recent')
                    
                if len(result):
                    for status in result:

                        totalTweets = totalTweets + 1
                        tweetcount = tweetcount + 1
                        if status.geo:
                            lon = status.geo['coordinates'][1]
                            lat = status.geo['coordinates'][0]
                            if lat<=47.3388 and lat>=36.6684 and lon>=5.5371 and lon<=18.7646:
                                loc = {'lon':lon,'lat':lat};
                                twitterDB.insert({'_id':status.id, 'loc':loc, 'text':status.text, 'created_at': status.created_at, 'from_user_id':status.user.id, 'from_user':status.user.screen_name})
                                savedTweets = savedTweets + 1
                                print str(status.created_at);
		        if datetime.now().hour == 3 and datetime.now().minute == 58:
		            print 'call to exit'
		            sys.exit(1)
					                
            except TweepError as t:
                print "TweepError:", t
                print "Going to sleep..."
                time.sleep(5*60)
                print "Resuming"
		
		print "Tweet Count Achieved"
        print "Going to sleep..."
        time.sleep(15)
        print "Resuming"
        tweetcount = 0
except SystemExit:
    print "Quit @ " + str(datetime.utcnow())
except KeyboardInterrupt:
    print "KeyboardInterrupt"
except BaseException as e:
    print "Error:", e
finally:
    connection.close()
    
print "Finished with " + str(totalTweets) + " tweets"
print "Saved " + str(savedTweets) + " tweets" 
