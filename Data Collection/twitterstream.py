import tweepy
from textwrap import TextWrapper
from AccessTokenHandler import AccessTokenHandler
import json, time
from datetime import datetime
from datetime import timedelta
from pymongo import Connection
import sys

connection = Connection()
username = 'xxxx'
password = 'xxxx'
connection['xxxx'].authenticate( username, password)

db = connection.sarleno
twitterDB = db.isGeo
twitterNoGeo = db.noGeo

count = 0

class StreamWatcherListener(tweepy.StreamListener):

    status_wrapper = TextWrapper(width=60, initial_indent='    ', subsequent_indent='    ')

    def on_status(self, status):
        try:
            if hasattr(status, 'coordinates') and status.coordinates:
                coo = status.coordinates
                lon = coo['coordinates'][0]
                lat = coo['coordinates'][1]
                if lat<=47.3388 and lat>=36.6684 and lon>=5.5371 and lon<=18.7646:
                    print 'isGeo: ' + str(status.created_at)
                    global count
                    count = count + 1
                    loc = {'lon':lon,'lat':lat};
                    twitterDB.insert({'_id':status.id, 'loc':loc, 'text':status.text, 'created_at': status.created_at, 'from_user_id':status.author.id, 'from_user':status.author.screen_name, 'to_user_id':status.in_reply_to_user_id, 'to_user':status.in_reply_to_screen_name})
            else:
                print 'noGeo: ' + str(status.created_at)
                twitterNoGeo.insert({'_id':status.id,'text':status.text, 'created_at': status.created_at, 'from_user_id':status.author.id, 'from_user':status.author.screen_name, 'to_user_id':status.in_reply_to_user_id, 'to_user':status.in_reply_to_screen_name})
        except Exception as e:
            print "Error: ", e
        if datetime.now().hour == 3 and datetime.now().minute == 58:
            print 'call to exit'
            sys.exit(1)


    def on_error(self, status_code):
        print 'An error has occurred! Status code = %s' % status_code
        return True

    def on_timeout(self):
        print 'Snoozing Zzzzzz'


handler = AccessTokenHandler()
auth = tweepy.OAuthHandler('xxxx', 'xxxx')
auth.set_access_token('xxxx', 'xxxx')

stream = tweepy.Stream(auth, StreamWatcherListener(), timeout=None)

start = datetime.utcnow()
delta = timedelta(microseconds=start.microsecond)
start = start - delta
loop = True

while loop:
    try:
        stream.filter(locations=[5.5371, 36.6684, 18.7646, 47.3388])
    except KeyboardInterrupt:
        print "KeyboardInterrupt"
        loop = False
    except BaseException as e:
        print "Error",e
        if str(e) == str(1):
            print 'Session expired, wait for cron to relaunch...'
            exit()
        else:
            print "Going to sleep."
            time.sleep(60*5)
            print "Resuming.\n"

end = datetime.utcnow()
delta = timedelta(microseconds=end.microsecond)
end = end - delta

try:
    with open('twitterLog.txt', 'a') as log:
        log.write("\"{fromDate}\"\t\"{untilDate}\"\t{total}\t{saved}\n".format(fromDate=start, untilDate=end, total="N/A", saved=count))
except IOError as er:
    print "Error writing to 'twitterLog.txt':", er

print "\nGoodbye\n%s tweets\n" %(count)